package com.bulleratz.bradio;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by maucher on 19.09.2017.
 */

public class RadioStationsPageAdapter extends FragmentStatePagerAdapter {

    private final int _buttonCount = 5;
    private RadioStationBouquet _bouquet = null;

    private BouquetManager.IBouquetManagerListener _bouqetManagerListener = new BouquetManager.IBouquetManagerListener() {
        @Override
        public void selectedBouquetChanged(RadioStationBouquet bouquet) {
            if (bouquet != null)
                setBouquet(bouquet);
        }

        @Override
        public void selectedStationChanged(RadioStation station, int stationIndex) {

        }

        @Override
        public void bouquetAdded(RadioStationBouquet bouquet) {

        }

        @Override
        public void bouquetRemoved(RadioStationBouquet bouquet) {

        }
    };

    private RadioStationBouquet.RadioStationBouqetObserver _bouquetListner = new RadioStationBouquet.RadioStationBouqetObserver() {
        @Override
        public void stationAdded(int index, RadioStation station) {
            notifyDataSetChanged();
            for (int i=0; i<getCount(); i++)
                updateFragment(i);
        }

        @Override
        public void stationRemoved(int index, RadioStation station) {
            notifyDataSetChanged();
            for (int i=0; i<getCount(); i++)
                updateFragment(i);
        }

        @Override
        public void nameChanged(String name) {

        }
    };

    @Override
    protected void finalize() throws Throwable {
        if (_bouquet != null)
            _bouquet.removeObserver(_bouquetListner);

        RadioService.getBouquetManager().removeBouquetManagerListener(_bouqetManagerListener);
        super.finalize();
    }

    private void setBouquet(RadioStationBouquet bouqet) {
        if (_bouquet != bouqet) {
            if (_bouquet != null)
                _bouquet.removeObserver(_bouquetListner);
            _bouquet = bouqet;
            if (_bouquet != null) {
                _bouquet.addObserver(_bouquetListner);
            }
            notifyDataSetChanged();
            for (int i=0; i<getCount(); i++)
                updateFragment(i);
        }
    }


    public RadioStationsPageAdapter(FragmentManager fm) {
        super(fm);
        RadioService.getBouquetManager().addBouquetManagerListener(_bouqetManagerListener);
        setBouquet(RadioService.getBouquetManager().getSelectedBouquet());

    }

    @Override
    public int getCount() {

        if (_bouquet != null)
            if (_bouquet.getCount() % _buttonCount == 0)
                return _bouquet.getCount() / _buttonCount;
            else
                return _bouquet.getCount() / _buttonCount + 1;
        return 0;
    }

    private StationsFragment[] _fragments = new StationsFragment[50];

    private StationsFragment updateFragment(int index) {
        StationsFragment fragment = null;
        if (_fragments[index] == null) {
            _fragments[index] = new StationsFragment();
            _fragments[index].setRetainInstance(true);
        }
        fragment = _fragments[index];
        int startIndex = index * _buttonCount;
        if (index == getCount() - 1) {
            fragment.setButtonCount(_bouquet.getCount() - index * _buttonCount);
        } else {
            fragment.setButtonCount(_buttonCount);
        }
        fragment.setButtonIndex(startIndex);
        fragment.updateButtons();
        return fragment;
    }

    @Override
    public Fragment getItem(int position) {
        if (position < getCount() && position >= 0) {
            StationsFragment fragment = null;
            if (_fragments[position] != null)
                return _fragments[position];
            return updateFragment(position);
        }
        return null;
    }

    public int getFragmentIndex(int stationIndex) {
        if (_bouquet == null || stationIndex < 0 || stationIndex >= _bouquet.getCount())
            return -1;
        int index = stationIndex / _buttonCount;
        if (index < 0 && index >= getCount())
            index = -1;
        return index;
    }
}
