package com.bulleratz.bradio;

import android.content.Context;

/**
 * Created by maucher on 15.11.2017.
 */


public class RadioStationButtonFactory {
    public RadioStationButton createButton(Context context) {
        return new WhiteRadioStationButton(context);
    }
}
