package com.bulleratz.bradio;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import greenmesh.settings.InfoBarSettingAdapter;
import greenmesh.settings.InfoBarSettingHandler;

public class InfoBarEditorDialog extends AppCompatActivity {
    ImageButton btnDown = null;
    ImageButton btnUp = null;
    private  InfoBarSettingAdapter adapter = null;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_bar_editor_dialog);
        Intent i = getIntent();
        String name = "";
        setTitle("Info Bar Settings");
        if (i.hasExtra("Name"))
            name = i.getStringExtra("Name");

        final ListView lvItems = findViewById(R.id.lvInfoBarItems);

        switch (name) {
            case UiSettings.TOP_CENTER_INFO_BAR_ITEMS:
                adapter = new InfoBarSettingAdapter(getBaseContext(), UiSettings.getTopCenterSettingsHandler());
                break;
            case UiSettings.TOP_LEFT_INFO_BAR_ITEMS:
                adapter = new InfoBarSettingAdapter(getBaseContext(), UiSettings.getTopLeftSettingsHandler());
                break;
            case UiSettings.TOP_RIGHT_INFO_BAR_ITEMS:
                adapter = new InfoBarSettingAdapter(getBaseContext(), UiSettings.getTopRightSettingsHandler());
                break;
            case UiSettings.BOTTOM_CENTER_INFO_BAR_ITEMS:
                adapter = new InfoBarSettingAdapter(getBaseContext(), UiSettings.getBottomCenterSettingsHandler());
                break;
            default:
                adapter = new InfoBarSettingAdapter(getBaseContext(), UiSettings.getTopLeftSettingsHandler());
                break; /* TODO: Error */
        }

        btnDown = findViewById(R.id.btnDown);
        btnUp = findViewById(R.id.btnUp);
        lvItems.setAdapter(adapter);
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lvItems.setSelection(i);
            }
        });
        adapter.addSelectedItemChangedObserver(new InfoBarSettingAdapter.SelectedItemChangedObserver() {
            @Override
            public void selectedItemChanged(int value) {
                lvItems.setItemChecked(adapter.getIndex(value), true);
            }
        });

        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.moveSelectedItemUp();
            }
        });

        btnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.moveSelectedItemDown();
            }
        });
    }
}

