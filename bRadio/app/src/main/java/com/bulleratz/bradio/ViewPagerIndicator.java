package com.bulleratz.bradio;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import greenmesh.ui.Theme;

/**
 * Created by maucher on 13.11.2017.
 */

public class ViewPagerIndicator extends LinearLayout {

    private ViewPager _viewPager = null;
    private PagerAdapter _pageAdapter = null;
    private Context _context = null;


    private ArrayList<ViewPagerIndicatorButton> _buttons = new ArrayList<ViewPagerIndicatorButton>();

    class ViewPagerIndicatorButton extends RelativeLayout {

        private Theme.ThemeObserver _themeObserver = new Theme.ThemeObserver() {
            @Override
            public void colorChanged(Theme theme, String ColorId) {
                updateColors();
            }


            @Override
            public void drawableChanged(String drawableId, Drawable drawable) {

            }
        };

        private ImageView _ivLedIndicator = null;
        private boolean _isChecked = false;
        public int index = 0;
        private Theme _theme = null;

        public ViewPagerIndicatorButton(Context context) {
            super(context);
            inflate(context, R.layout.page_indicator_button, this);
            _ivLedIndicator = (ImageView)findViewById(R.id.ivLedIndicator);
            _theme = CurrentTheme.getInstance();
            _theme.addObserver(_themeObserver);
            updateColors();

            this.setClickable(true);
        }

        public void setChecked(boolean checked) {
            _isChecked = checked;
            updateColors();
        }

        private void updateColors() {
            if (_theme != null) {
                if (_isChecked) {
                    _ivLedIndicator.setColorFilter(_theme.getColor(CurrentTheme.THEME_COLOR_SELECTED));
                } else {
                    _ivLedIndicator.setColorFilter(_theme.getColor(CurrentTheme.THEME_COLOR_NOT_SELECTED));
                }
            }
        }
    }

    private DataSetObserver _dataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            updateButtons();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
            updateButtons();
        }
    };

    private ViewPager.OnPageChangeListener _pageChangedListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            for (int i=0; i<_buttons.size(); i++) {
                _buttons.get(i).setChecked(i == position);
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public ViewPagerIndicator(Context context) {
        super(context);
        init();
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void init()
    {
    }

    void updateButtons()
    {
        while (getChildCount() > 0)
            removeView(getChildAt(0));
        setWeightSum(10f);
        _buttons.clear();
        if (_pageAdapter != null) {
            for (int i=0; i<_pageAdapter.getCount(); i++) {
                LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.setMargins(5,5,5,5);
                params.weight = 10f / _pageAdapter.getCount();
                ViewPagerIndicatorButton button = new ViewPagerIndicatorButton(getContext());
                addView(button, params);
                _buttons.add(button);
                if (_viewPager.getCurrentItem() == i) {
                    button.setChecked(true);
                }
                button.index = i;
                button.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (_viewPager != null)
                            _viewPager.setCurrentItem(((ViewPagerIndicatorButton)view).index);
                    }
                });
            }

        }
    }

    public void setViewPager(ViewPager pager) {
        if (_viewPager != pager) {
            // Listener entfernen
            if (_viewPager != null)
                _viewPager.removeOnPageChangeListener(_pageChangedListener);
            _viewPager = pager;
            //Listener hinzufügen
            if (_viewPager != null)
                _viewPager.addOnPageChangeListener(_pageChangedListener);
        }
        if (_viewPager != null) {
            if (_pageAdapter != _viewPager.getAdapter()) {
                //Alten Oberserver de-registrieren
                if (_pageAdapter != null)
                    _pageAdapter.unregisterDataSetObserver(_dataSetObserver);
                _pageAdapter = _viewPager.getAdapter();
                //Oberserver neu registrieren
                if (_pageAdapter != null)
                    _pageAdapter.registerDataSetObserver(_dataSetObserver);
            }
        }
        updateButtons();
    }
}
