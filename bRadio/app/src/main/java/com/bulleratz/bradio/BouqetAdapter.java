package com.bulleratz.bradio;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class BouqetAdapter extends ArrayAdapter<RadioStation> {

    RadioStationBouquet.RadioStationBouqetObserver _observer= new RadioStationBouquet.RadioStationBouqetObserver() {
        @Override
        public void stationAdded(int index, RadioStation station) {
            notifyDataSetChanged();
        }

        @Override
        public void stationRemoved(int index, RadioStation station) {
            notifyDataSetChanged();
        }

        @Override
        public void nameChanged(String name) {
            notifyDataSetChanged();
        }
    };

    private class ViewHolder {
         public ImageView ivIcon = null;
         public TextView tvName = null;
         public TextView tvFreq = null;
         public LinearLayout background = null;
         public RadioStation station = null;
    }

    private ArrayList<WeakReference<ViewHolder>> _viewHolders = new ArrayList<>();
    private RadioStationBouquet _bouquet = null;
    private Context _context = null;
    private RadioStation _selectedStation = null;

    public BouqetAdapter(@NonNull Context context, RadioStationBouquet bouquet) {
        super(context, R.layout.bouquet_adapter_item);
        _bouquet = bouquet;
        _bouquet.addObserver(_observer);
        _context = context;

    }

    public void setSelected(RadioStation station) {
        _selectedStation = station;
        for (int i=0; i<_viewHolders.size(); i++) {
            ViewHolder holder = _viewHolders.get(i).get();
            if (holder == null) {
                _viewHolders.remove(i);
                i--;
            }
            else if (holder.station == station) {
                holder.background.setBackgroundColor(Color.argb(0xff, 0x88, 0x88, 0x88));
            }
            else {
                holder.background.setBackgroundColor(Color.argb(00, 0x00, 0x00, 0x00));
            }

        }
    }

    @Override
    public int getCount() {
        return _bouquet.getCount();
    }

    @Nullable
    @Override
    public RadioStation getItem(int position) {
        return _bouquet.getStation(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.bouquet_adapter_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView)convertView.findViewById(R.id.tvName);
            viewHolder.tvFreq = (TextView)convertView.findViewById(R.id.tvFreq);
            viewHolder.ivIcon = (ImageView)convertView.findViewById(R.id.ivIcon);
            viewHolder.background = (LinearLayout)convertView.findViewById(R.id.bouquet_background_layout) ;
            convertView.setTag(viewHolder);
            _viewHolders.add(new WeakReference<ViewHolder>(viewHolder));
        }
        viewHolder = (ViewHolder)convertView.getTag();

        viewHolder.tvName.setText(_bouquet.getStation(position).getDisplayName());
        viewHolder.tvFreq.setText(Double.toString(_bouquet.getStation(position).getFrequency() / 1000000.0));
        viewHolder.ivIcon.setImageBitmap(_bouquet.getStation(position).getBitmap());
        viewHolder.station = _bouquet.getStation(position);
        if (viewHolder.station == _selectedStation) {
            viewHolder.background.setBackgroundColor(Color.argb(0xff, 0x88, 0x88, 0x88));
        } else {
            viewHolder.background.setBackgroundColor(Color.argb(00, 0x00, 0x00, 0x00));
        }

        return convertView;
    }

    RadioStation getSelectedStation() {
        return _selectedStation;
    }
}
