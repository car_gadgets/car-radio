package com.bulleratz.bradio;

import java.util.ArrayList;

public class RdsRepository {

    private static int EXPIRATION_TIME = 60000;

    public class RdsData
    {
        public int frequency = 0;
        public int pi = 0;
        public String name = "";
        public String title = "";
        public long updateTime = 0;

        public RdsData clone() {
            RdsData cloneData = new RdsData();
            cloneData.frequency = frequency;
            cloneData.pi = pi;
            cloneData.name = name;
            cloneData.title = title;
            return cloneData;
        }

        public boolean isValid() {
            return (updateTime + EXPIRATION_TIME) > System.currentTimeMillis();
        }

        public String toString() {
            return "PI: " + pi + "\n Freq: " + frequency + "\n Name: " + name + " \nPSN: " + title;
        }
    }

    private ArrayList<RdsData> _data = new ArrayList<>();

    public void update(int frequency, int pi, String name, String title) {
        if (name == null)
            name = "";
        if (title == null)
            title = "";
        boolean found = false;
        if (pi == 0) { // Find by frequency */
            for (int i=0; i<_data.size(); i++) {
                RdsData data = _data.get(i);
                if (data.frequency == frequency) {
                    //if (data.isValid()) { /* If data is valid don't override with empty strings */
                    //                       /* could also  be checked in Service (setPsn(psn, boolean writeTORepo)... */
                    //    if (name.isEmpty())
                    //        name = data.name;
                    //    if (title.isEmpty())
                    //        title = data.title;
                    //}
                    updateData(data, data.frequency, data.pi, name, title);
                    if (data.pi != 0) {
                        update(0, data.pi, name, title);
                    }
                    found = true;
                    break;
                }
            }
        } else { /*find by PI */
            for (int i=0; i<_data.size(); i++) {
                RdsData data = _data.get(i);
                if (data.pi == pi) {
                    updateData(data, data.frequency, data.pi, name, title);
                    found = true;
                }
            }
        }
        if (!found) {
            RdsData data = new RdsData();
            data.frequency = frequency;
            data.pi = pi;
            data.title = title;
            data.name = name;
            data.updateTime = System.currentTimeMillis();
            _data.add(data);
        }
    }

    private void updateData(RdsData data, int freq, int pi, String name, String title) {
        data.frequency = freq;
        data.pi = pi;
        if (name != null && !name.isEmpty())
            data.name = name;
        if (title != null && !title.isEmpty())
            data.title = title;
        data.updateTime = System.currentTimeMillis();
    }

    public RdsData getDataByFreq(int freq) {
        RdsData data = new RdsData();
        for (int i=0; i<_data.size(); i++) {
            RdsData d = _data.get(i);
            if (d.isValid() && d.frequency == freq) {
                data = d.clone();
                break;
            }
        }
        return data;
    }

    public RdsData getDataByPu(int pi) {
        RdsData data = new RdsData();
        for (int i=0; i<_data.size(); i++) {
            if (_data.get(i).isValid() && _data.get(i).pi == pi) {
                data = _data.get(i).clone();
                data.frequency = 0;
                break;
            }
        }
        return data;
    }

    public String toString() {
        String ret = "";
        for (int i=0; i<_data.size(); i++) {
            ret += _data.get(i).toString() + "\n\n";
        }
        return ret;
    }
}
