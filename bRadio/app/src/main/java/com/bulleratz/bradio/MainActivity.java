package com.bulleratz.bradio;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.IBinder;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import greenmesh.settings.SettingsRepository;
import greenmesh.ui.Theme;


public class MainActivity extends FragmentActivity {

    //Station View
    TextView _stationName = null;
    TextView _tvTitle = null;
    ImageButton _btnNext = null;
    ImageButton _btnPrev = null;
    ImageView _ivStationImage = null;
    RadioStation _currentStation = null;
    ImageButton _btnSeekNext = null;
    ImageButton _btnSeekPrev = null;
    //RadioService _radioService = null;
    RadioService _serviceBinder = null;
    private ViewPager _stationsPager = null;
    public static MainActivity _instance = null;
    ToggleButton _btnFm = null;
    ToggleButton _btnAm = null;
    ToggleButton _btnTa = null;
    ToggleButton _btnPower = null;
    ToggleButton _btnAf = null;
    ToggleButton _btnSettings = null;
    boolean _radioOn = true;
    ViewPagerIndicator _statiosPagerIndicator = null;
    View _accentBarTop = null;
    View _accentBarBottom = null;
    private LinearLayout _rootLayout = null;
    private Handler _clockHandler = null;
    private View _layoutUpperBar = null;
    private View _layoutBottomBar = null;
    private FrequencyBar _freqBar = null;
    private boolean _freqBarVisible = false;
    private RadioStationBouquet _selectedBouquet = null;
    private float _stationFontSize = 0;
    private float _titleFontSize = 0;

    private final SettingsRepository.SettingsRepositoryObserver _uiSettingsObserver = new SettingsRepository.SettingsRepositoryObserver() {
        @Override
        public void valueChanged(String name, Object value) {
            if (name.equals(UiSettings.FREQ_BAR_VISIBLE)) {
                MainActivity.this.showFrequencyBar(UiSettings.getFreqBarVisible());
            }
            if (name.equals(UiSettings.FULLSCREEN)) {
                recreate();
            }
        }
    };



    private static final int SET_CURRENT_PAGE = 0;

    Handler _handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == SET_CURRENT_PAGE) {
                if (_stationsPageAdapter != null && _stationsPager != null) {
                    int fragment = _stationsPageAdapter.getFragmentIndex(RadioService.getBouquetManager().getSelectedStationIndex());
                    if (fragment >= 0 && fragment < _stationsPageAdapter.getCount())
                        _stationsPager.setCurrentItem(fragment);
                }
            }
        }
    };

    IRadioStationListener _stationListener = new IRadioStationListener() {
        @Override
        public void radioStationChanged(RadioStation sender) {
            if (sender == _currentStation)
            {
                refreshStationView();
            }
        }
    };

    private void refreshStationView() {
        if (_currentStation != null) {
            if (_stationName != null) {
                _stationName.setText(_currentStation.getDisplayName());
            }

            if (_tvTitle != null)
                _tvTitle.setText(_currentStation.getCurrentTitle());
            if (_ivStationImage != null) {
                _ivStationImage.setImageDrawable(_currentStation.getImage());
                _ivStationImage.setColorFilter(Color.rgb(50, 50, 50), android.graphics.PorterDuff.Mode.MULTIPLY);
            }
        }
    }

    private void showFrequencyBar(boolean show) {
        UiSettings.setFreqBarVisible(show);
        _freqBarVisible = show;
        if (_freqBar != null) {
            if (show)
                _freqBar.setVisibility(View.VISIBLE);
            else
                _freqBar.setVisibility(View.INVISIBLE);
        }
        if (_stationName != null) {
            if (show)
                _stationName.setVisibility(View.INVISIBLE);
            else
                _stationName.setVisibility(View.VISIBLE);
        }
    }

    public void toogleFreqBar() {
        showFrequencyBar(!_freqBarVisible);
    }

    BouquetManager.IBouquetManagerListener _bouquetListener = new BouquetManager.IBouquetManagerListener() {
        @Override
        public void selectedBouquetChanged(RadioStationBouquet bouquet) {
            _selectedBouquet = bouquet;
            if (_selectedBouquet != null) {
                Settings.setBouquetName(bouquet.getName());
            }
            Settings.storeSettings(getApplicationContext());
        }

        @Override
        public void selectedStationChanged(RadioStation station, int stationIndex) {
            Settings.setStationIndex(stationIndex);
            Settings.setFrequency(station.getFrequency());
            Settings.storeSettings(getApplicationContext());
            /*Grund: Wharend der Listener benachritigt wird, wird durch das Ändern des Fragments ein anderer Listener abgemeldet (anderes Fragment wird zerstört). Das führt zu einer Excpetion (Auflistung geändert)*/
            _handler.sendEmptyMessage(SET_CURRENT_PAGE);

        }

        @Override
        public void bouquetAdded(RadioStationBouquet bouquet) {

        }

        @Override
        public void bouquetRemoved(RadioStationBouquet bouquet) {

        }
    };

    IRadioServiceListener _listener;

    {
        _listener = new IRadioServiceListener() {
            @Override
            public void frequencyChanged(int frequency) {
                boolean isFm = true;
                if (_serviceBinder != null)
                    isFm = _serviceBinder.isFm();
                _btnAm.setChecked(!isFm);
                _btnFm.setChecked(isFm);
                _freqBar.setFreq(frequency);
            }

            @Override
            public void psnAvailable(String psn) {

            }

            @Override
            public void titleAvailable(String title) {

            }

            @Override
            public void ptyChanged(int pty) {
                setPty();
            }

            @Override
            public void piChanged(int pi) {
            }

            @Override
            public void stereoChanged(boolean stereo) {
                updateStereo(stereo);
            }

            @Override
            public void strengthChanged(int strength) {

            }

            @Override
            public void seekStart() {

            }

            @Override
            public void autoseekStart() {

            }

            @Override
            public void seekFound() {

            }

            @Override
            public void autoseekFound() {

            }

            @Override
            public void seekEnd() {

            }

            @Override
            public void autoseekEnd(RadioStationBouquet seekResult) {

            }

            @Override
            public void tpChanged(boolean tp) {
                updateTp(tp);
            }

            @Override
            public void taChanged(boolean ta) {
                updateTa(ta);
            }

            @Override
            public void bandChanged() {
                configureFreqBar();
            }
        };
    }

    private void configureFreqBar() {
        if (_freqBar != null && _serviceBinder != null) {
            _freqBar.setConf(_serviceBinder.getMinFrequency(), _serviceBinder.getMaxFrequency(), _serviceBinder.getFrequencyStep(), _serviceBinder.isAm());
            _freqBar.setFreq(_serviceBinder.getFrequency());
        }
    }

    private void updateTa(boolean ta) {
    }

    private void updateTp(boolean tp) {
    }

    private void updateStereo(boolean stereo) {
    }

    private ServiceConnection mConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            _serviceBinder = ((RadioServiceBinder)iBinder).getService();
            Log.d("BRadio", "OnServiceConnected");
            _serviceBinder.setRadioOn(_radioOn);
            _serviceBinder.addRadioServiceListener(_listener);


            updateTa(_serviceBinder.getTa());
            updateTp(_serviceBinder.getTp());
            updateStereo(_serviceBinder.isStereo());
            setPty();

            UiSettings.init(getApplicationContext());
            Settings.restoreSettings(getApplicationContext());
            RadioService.getBouquetManager().selectBouquet(Settings.getBouquetName());
            RadioService.getBouquetManager().selectStation(Settings.getStationIndex());
            _serviceBinder.setFrequency(Settings.getFrerquency());
            showFrequencyBar(UiSettings.getFreqBarVisible());

            _selectedBouquet = RadioService.getBouquetManager().getSelectedBouquet();
            RadioService.getBouquetManager().addBouquetManagerListener(_bouquetListener);

            boolean isFm = true;

            //TODO: Auslagern
            if (_serviceBinder != null) {
                isFm = _serviceBinder.isFm();
                _serviceBinder.setTaEnabled(Settings.getTaOn());
                _serviceBinder.setStereoEnabled(Settings.getSteroOn());
                _serviceBinder.setLocEnabled(false);
            }
            _btnAm.setChecked(!isFm);
            _btnFm.setChecked(isFm);
            _btnTa.setChecked(Settings.getTaOn());
            configureFreqBar();
            setAf();


        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d("BRadio", "OnServiceDisconnected");
            mConnection = null;
            unsetService();
        }
    };

    private void setAf() {
        boolean afOn = Settings.getAfOn();
        if (_serviceBinder != null)
            _serviceBinder.setAfEnabled(afOn);
        if (_btnAf != null)
            _btnAf.setChecked(afOn);
    }

    private void unsetService()
    {
        RadioService.getBouquetManager().removeBouquetManagerListener(_bouquetListener);
        _radioOn = false;
        if (_serviceBinder != null) {
            _serviceBinder.setRadioOn(_radioOn);
        }
        if (mConnection != null) {
            unbindService(mConnection);
        }
        _serviceBinder = null;
    }

    private RadioStationsPageAdapter _stationsPageAdapter = null;

    private PagerAdapter _mainPageAdapter = null;

    private final int STORAGE_PERMISSION_REQUEST_CODE = 101;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults)
    {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length == 0
                    || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finish();
            }
        }
    }

    void requestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _instance = this;

        UiSettings.init(getApplicationContext());
        Settings.restoreSettings(getApplicationContext());

        if (UiSettings.getFullscreen()) {
            // remove title
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }


        Thread.currentThread().setUncaughtExceptionHandler(new CrashHandler(getBaseContext()));

        requestPermissions();


        setContentView(R.layout.activity_main);
        _rootLayout = (LinearLayout)findViewById(R.id.mainActivityRootLayout);
        _stationsPager = (ViewPager)findViewById(R.id.vpStations);
        _stationsPageAdapter = new RadioStationsPageAdapter(getSupportFragmentManager());
        _stationsPager.setAdapter(_stationsPageAdapter);
        _btnAm = (ToggleButton)findViewById(R.id.btnAm);
        _btnFm = (ToggleButton)findViewById(R.id.btnFm);
        _btnTa = (ToggleButton)findViewById(R.id.btnTa);
        _btnPower = (ToggleButton)findViewById(R.id.btnPower);
        _btnAf = (ToggleButton)findViewById(R.id.btnAf);
        _btnSettings = (ToggleButton)findViewById(R.id.btnSettings);
        _statiosPagerIndicator = (ViewPagerIndicator)findViewById(R.id.stationsPagerIndicator);
        _statiosPagerIndicator.setViewPager(_stationsPager);
        _stationName = (TextView)findViewById(R.id.tvStationName);

        _tvTitle = (TextView)findViewById(R.id.tvTitle);
        _btnPrev = (ImageButton)findViewById(R.id.btnPrev);
        _btnNext = (ImageButton)findViewById(R.id.btnNext);
        _btnSeekNext = (ImageButton)findViewById(R.id.btnSeekNext);
        _btnSeekPrev = (ImageButton)findViewById(R.id.btnSeekPrev);
        _accentBarTop = findViewById(R.id.accentBarTop);
        _accentBarBottom = findViewById(R.id.accentBarBottom);
        _layoutBottomBar = (View)findViewById(R.id.layoutBottomBar);
        _layoutUpperBar = (View)findViewById(R.id.layoutTopBar);
        _freqBar = (FrequencyBar)findViewById(R.id.freqBar);
        _stationFontSize = _stationName.getTextSize();
        _titleFontSize = _tvTitle.getTextSize();

        UiSettings.init(getApplicationContext());
        _stationName.setTextSize(TypedValue.COMPLEX_UNIT_PX, _stationFontSize * UiSettings.getFontScale());
        _tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, _titleFontSize * UiSettings.getFontScale());

        refreshStationView();



        _freqBar.setOnFreqBarChangeListener(new FrequencyBar.OnFreqBarChangeListener() {
            @Override
            public void onMoveTrackingTouch(FrequencyBar freqBarView3) {

            }

            @Override
            public void onStartTrackingTouch(FrequencyBar freqBarView3) {

            }

            @Override
            public void onStopTrackingTouch(FrequencyBar freqBarView3) {
                if (_serviceBinder != null)
                    _serviceBinder.setFrequency(_freqBar.getRealFreq());
            }
        });

        _btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_serviceBinder != null)
                    _serviceBinder.increaseFreq();
                }
        });

        _btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_serviceBinder != null)
                    _serviceBinder.decreaseFreq();
            }
        });

        _btnSeekNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_serviceBinder != null)
                    _serviceBinder.seekNext();
            }
        });

        _btnSeekPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_serviceBinder != null)
                    _serviceBinder.seekPrev();
            }
        });

        RadioService.getBouquetManager().addBouquetManagerListener(new BouquetManager.IBouquetManagerListener() {
            @Override
            public void selectedBouquetChanged(RadioStationBouquet bouquet) {
            }

            @Override
            public void selectedStationChanged(RadioStation station, int stationIndex) {
                if (station != null && station != _currentStation) {
                    if (_currentStation != null)
                        _currentStation.removeRadioStationListener(_stationListener);
                    _currentStation = station;
                    if (_currentStation != null)
                        _currentStation.addRadioStationListener(_stationListener);
                }
                _stationListener.radioStationChanged(_currentStation);
            }

            @Override
            public void bouquetAdded(RadioStationBouquet bouquet) {

            }

            @Override
            public void bouquetRemoved(RadioStationBouquet bouquet) {

            }
        });

        if (_clockHandler != null) {
            _clockHandler.sendEmptyMessage(0);
        }

        if (_serviceBinder == null) {
            Intent intent = new Intent(this, RadioService.class);
            bindService(intent, mConnection, BIND_AUTO_CREATE);
        }

        _btnPower.setChecked(_radioOn);

        if (_serviceBinder != null) {
            _serviceBinder.setLocEnabled(false);
            _serviceBinder.setStereoEnabled(Settings.getSteroOn());
        }
        setPty();
        setAf();

        _btnFm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_btnFm.isChecked()) {
                    if (_serviceBinder != null)
                        _serviceBinder.setBandFm();
                } else {
                    _btnFm.setChecked(true);
                }
            }
        });

        _btnAm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_btnAm.isChecked()) {
                    if (_serviceBinder != null && _serviceBinder.isFm())
                        _serviceBinder.setBandAm();
                } else {
                    _btnAm.setChecked(true);
                }
            }
        });

        _btnTa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_serviceBinder != null) {
                    _serviceBinder.setTaEnabled(_btnTa.isChecked());
                }
                Settings.setTaOn(_btnTa.isChecked());
                Settings.storeSettings(getApplicationContext());
            }
        });

        _btnPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_serviceBinder != null) {
                    //Intent intent = new Intent(getBaseContext(), RadioEventActivity.class);
                   // startActivity(intent);
                    finish();
                } else {
                    finish();
                }
            }
        });
        _btnPower.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                _radioOn = !_radioOn;
                if (_serviceBinder != null)
                    _serviceBinder.setRadioOn(_radioOn);
                _btnPower.setChecked(_radioOn);
                RadioService.getBouquetManager().saveBouquets();
                return true;
            }
        });
        _btnAf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Settings.setAfOn(_btnAf.isChecked());
                Settings.storeSettings(getApplicationContext());
                setAf();
            }
        });

        _btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                String stereoString = "Enable Stereo";
                if (_serviceBinder.isStereoEnabled())
                    stereoString = "Disable Stereo";
                String items[] = new String[] { getResources().getString(R.string.edit_bouquets), getResources().getString(R.string.manage_bouquets), getResources().getString(R.string.edit_theme),  getResources().getString(R.string.toggle_frequency)  /*"UI Settings"*/, getResources().getString(R.string.toggle_fullscreen), stereoString, getResources().getString(R.string.setFontSize), "Autoscan", "Advanced..."};
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       if (i == 0) {
                           Intent intent = new Intent(getBaseContext(), BouquetEditor.class);
                           startActivity(intent);
                       }
                       if ( i == 1)
                       {
                           Intent intent = new Intent(getBaseContext(), BoquetManagerEditor.class);
                           startActivity(intent);
                       }
                       if ( i == 2) {
                           //showThemeDialog();
                           Intent intent = new Intent(getBaseContext(), ThemeEditor.class);
                           startActivity(intent);

                       }
                       if (i == 3) {
                           UiSettings.setFreqBarVisible(!UiSettings.getFreqBarVisible());
                       }
                       if (i == 4) {
                           UiSettings.setFullscreen(!UiSettings.getFullscreen());
                       }
                       if (i == 5) {
                           Settings.setStereoOn(!Settings.getSteroOn());
                           Settings.storeSettings(getApplicationContext());
                           if (_serviceBinder != null) {
                               _serviceBinder.setStereoEnabled(Settings.getSteroOn());
                           }
                       }
                       if (i == 6) {

                           showFontDialog();
                       }
                       if (i == 7) {
                           Intent intent = new Intent(getBaseContext(), AutoscanDialog.class);
                           startActivity(intent);
                       }
                       if (i == 8) {
                           Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
                           startActivity(intent);
                       }
                    }
                });
                builder.show();

            }
        });



        initTheme();
        UiSettings.addObserver(_uiSettingsObserver);

    }



    void showFontDialog() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(getResources().getString(R.string.setFontSize));
        alert.setMessage("");

        LinearLayout linear=new LinearLayout(this);

        linear.setOrientation(LinearLayout.VERTICAL);
        final TextView text=new TextView(this);
        text.setText(getResources().getString(R.string.scaling) + ": " + Float.toString(UiSettings.getFontScale()));
        text.setPadding(10, 10, 10, 10);

        final SeekBar seek=new SeekBar(this);

        seek.setMax(10);
        seek.setProgress((int)(UiSettings.getFontScale() * 10) - 5);

        linear.addView(seek);
        linear.addView(text);

        alert.setView(linear);

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                float scale = (seek.getProgress() + 5) / 10.0f;
                text.setText(getResources().getString(R.string.scaling) + ": " + Float.toString(scale));
                _tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, _titleFontSize * scale);
                _stationName.setTextSize(TypedValue.COMPLEX_UNIT_PX, _stationFontSize * scale);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        alert.setPositiveButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int id)
            {
                float scale = (seek.getProgress() + 5) / 10.0f;
                _tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, _titleFontSize * scale);
                _stationName.setTextSize(TypedValue.COMPLEX_UNIT_PX, _stationFontSize * scale);
                UiSettings.setFontScale(scale);;
            }
        });

        alert.setNegativeButton(getResources().getString(R.string.cancel),new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int id)
            {
                float scale = UiSettings.getFontScale();
                _tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, _titleFontSize * scale);
                _stationName.setTextSize(TypedValue.COMPLEX_UNIT_PX, _stationFontSize * scale);
            }
        });

        alert.show();
    }

    long _lastKeyBack = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        long pressTime = System.currentTimeMillis();
        if (keyCode == KeyEvent.KEYCODE_BACK && (pressTime - _lastKeyBack) > 2000 && UiSettings.getBackButtonAction() == UiSettings.BACK_BUTTON_ACTION_NOTIFY) {
            Toast toast = Toast.makeText(getBaseContext(), R.string.press_again_exit, Toast.LENGTH_SHORT);
            toast.show();
            _lastKeyBack = pressTime;
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK && UiSettings.getBackButtonAction() == UiSettings.BACK_BUTTON_ACTION_HIDE) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showThemeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getResources().getString(R.string.select_theme));
        File file = new File(Constants.THEME_STORAGE_PATH);
        ArrayList<String> themeFiles = new ArrayList<>();
        themeFiles.add(getResources().getString(R.string.none_item));
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();
            for (int i=0; i<files.length; i++) {
                if (files[i].getName().toUpperCase().endsWith(".JSON")) {
                    themeFiles.add(files[i].getName());
                }
            }
        }

        final String[] strArr = new String[themeFiles.size()];
        for (int i=0; i<strArr.length; i++) {
            strArr[i] = themeFiles.get(i);
        }

        builder.setItems(strArr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0)
                    CurrentTheme.getInstance().setTheme("");
                else
                    CurrentTheme.getInstance().setTheme(Constants.THEME_STORAGE_PATH + strArr[i]);
                Settings.storeSettings(MainActivity.this.getApplicationContext());

            }
        });
        builder.show();
    }

    private Theme _currentTheme = null;

    private void setPty() {
        /*if (_serviceBinder != null && _tvPty != null)
        {
            int pty = _serviceBinder.getPty();
            String[] arr = getResources().getStringArray(R.array.pty_names);
            if (arr != null && pty >= 0 && pty < arr.length) {
                _tvPty.setText(arr[pty]);
            }
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (_serviceBinder != null)
            _serviceBinder.setRadioOn(_radioOn);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (_serviceBinder != null)
            _serviceBinder.setRadioOn(_radioOn);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (_serviceBinder != null)
            _serviceBinder.setRadioOn(_radioOn);
    }

    @Override
    protected void onDestroy() {
        Log.d("BRadio", "MainActivity destroyed");
        unsetService();
        UiSettings.removeObserver(_uiSettingsObserver);
        super.onDestroy();
    }

    private Theme.ThemeObserver _obsrver = new Theme.ThemeObserver() {
        @Override
        public void colorChanged(Theme theme, String ColorId) {
            if (_accentBarTop != null)
                _accentBarTop.setBackgroundColor(theme.getColor(CurrentTheme.THEME_COLOR_ACCENT));
            if (_accentBarBottom != null)
                _accentBarBottom.setBackgroundColor(theme.getColor(CurrentTheme.THEME_COLOR_ACCENT));
            if (_layoutBottomBar != null)
                _layoutBottomBar.setBackgroundColor(theme.getColor(CurrentTheme.THEME_COLOR_BACKGROUND_LIGHT));
            if (_layoutUpperBar != null)
                _layoutUpperBar.setBackgroundColor(theme.getColor(CurrentTheme.THEME_COLOR_BACKGROUND_LIGHT));
            if (_rootLayout != null) {
                _rootLayout.setBackgroundColor(theme.getColor(CurrentTheme.THEME_COLOR_BACKGROUND));
            }
        }


        @Override
        public void drawableChanged(String drawableId, Drawable drawable) {
            if (_rootLayout != null) {
                _rootLayout.setBackground(drawable);
                if (drawable == null) {
                    _rootLayout.setBackgroundColor(_currentTheme.getColor(CurrentTheme.THEME_COLOR_BACKGROUND));
                }
            }
        }
    };

    private void initTheme() {
        UiSettings.init(getApplicationContext());
        Settings.restoreSettings(getApplicationContext());
        _currentTheme = CurrentTheme.getInstance();
        _currentTheme.addObserver(_obsrver);
        _obsrver.colorChanged(CurrentTheme.getInstance(), ""); /* TODO: Real update */
        _obsrver.drawableChanged(CurrentTheme.THEME_DRAWABLE_WALLPAPER, _currentTheme.getDrawable(CurrentTheme.THEME_DRAWABLE_WALLPAPER));
    }
}
