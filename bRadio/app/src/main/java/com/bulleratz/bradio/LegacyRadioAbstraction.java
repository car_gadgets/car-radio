package com.bulleratz.bradio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class LegacyRadioAbstraction extends RadioAbstractionLayer {

    private IntentFilter intentFilter = null;

    private HashMap<String, Handler> _handlers = new HashMap<>();
    private Context _context = null;
    private AudioManager _audioMgr = null;

    public LegacyRadioAbstraction(Context context) {
        _context = context;

        intentFilter = new IntentFilter();
        intentFilter.addAction("com.hct.radio.report");
        intentFilter.addAction("ACTION_IVCAR_OPERATION");
        intentFilter.addAction("com.microntek.irkeyDown");
        intentFilter.addAction(Intent.ACTION_SHUTDOWN);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_MEDIA_BUTTON);

        if (_context != null) {
            _receiver = _rcvr;
            context.registerReceiver(_receiver, intentFilter);
        }

        _audioMgr = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
    }

    private void notifyKeyDown(Bundle bundle) {

    }

    private void notifyRadio(Bundle bundle) {
        if (_handlers.containsKey("Radio")) {
            Message msg = Message.obtain();
            msg.setData(bundle);
            msg.obj = "Radio";
            _handlers.get("Radio").dispatchMessage(msg);
        }
    }

    BroadcastReceiver _receiver = null;
    BroadcastReceiver _rcvr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = new Bundle();
            String action = intent.getAction();
            if (action.equals("com.hct.radio.report")) {
                if (intent.hasExtra("freq")) {

                    int newFreq = intent.getIntExtra("freq", 87500000);
                    bundle.putString("type", "freq");
                    bundle.putInt("value", newFreq);
                    notifyRadio(bundle);
                    //if (newFreq != _frequency) {
                    //    setFrequencyPrivate(newFreq);
                    //}
                } else if (intent.hasExtra("title")) {
                    String title = "";
                    byte[] b = intent.getByteArrayExtra("title");
                    bundle.putString("type", "rt");
                    bundle.putByteArray("value", b);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("psn")) {
                    byte[] b = intent.getByteArrayExtra("psn");
                    bundle.putString("type", "psn");
                    bundle.putByteArray("value", b);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("pty")) {
                    int pty = intent.getIntExtra("pty", 0);
                    bundle.putString("type", "pty");
                    bundle.putInt("value", pty);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("pi")) {
                    int pi = intent.getIntExtra("pi", 0);
                    bundle.putString("type", "pi");
                    bundle.putInt("value", pi);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("stereo")) {
                    int st = intent.getIntExtra("stereo", 0);
                    bundle.putString("type", "stereo");
                    bundle.putInt("value", st);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("tp")) {
                    int tp = intent.getIntExtra("tp", 0);
                    bundle.putString("type", "tp");
                    bundle.putInt("value", tp);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("ta")) {
                    int ta = intent.getIntExtra("ta", 0);
                    bundle.putString("type", "ta");
                    bundle.putInt("value", ta);
                    notifyRadio(bundle);
                }
                else if (intent.hasExtra("strength")) {
                    int strength = intent.getIntExtra("strength", 0);
                    bundle.putString("type", "strength");
                    bundle.putInt("value", strength);
                    notifyRadio(bundle);
                } else if (intent.hasExtra("seek")) {
                    String op = intent.getStringExtra("seek");
                    if (op.equals("start"))
                    {
                        bundle.putString("type", "seek_start");
                        notifyRadio(bundle);
                    } else if (op.equals("autostart"))
                    {
                        bundle.putString("type", "seek_start_auto");
                        notifyRadio(bundle);
                    } else if (op.equals("found"))
                    {
                        bundle.putString("type", "seek_found");
                        notifyRadio(bundle);
                    } else if (op.equals("autofound"))
                    {
                        bundle.putString("type", "seek_found_auto");
                        notifyRadio(bundle);
                    } else if (op.equals("end"))
                    {
                        bundle.putString("type", "seek_end");
                        notifyRadio(bundle);
                    } else if (op.equals("autoend"))
                    {
                        bundle.putString("type", "seek_end_auto");
                        notifyRadio(bundle);
                    }
                } /*else if (intent.hasExtra("power")) {
                    Log.d("BRadioService", "Redio Power Received");
                    if (RadioService.this.getParameters("av_channel=").equals("fm")) {
                        setRadioOn(false);
                    } else {
                        setRadioOn(true);
                    }
                }*/ else if (intent.hasExtra("string_title")) {
                    bundle.putString("type", "rt");
                    bundle.putByteArray("value", intent.getStringExtra("string_title").getBytes());
                    notifyRadio(bundle);
                }else if (intent.hasExtra("string_psn")) {
                    bundle.putString("type", "psn");
                    bundle.putByteArray("value", intent.getStringExtra("string_psn").getBytes());
                    notifyRadio(bundle);
                }
            } else if (action.equals("ACTION_IVCAR_OPERATION") && intent.hasExtra("OPERATION")) {
                String op = intent.getStringExtra("OPERATION");
                Intent it;
            /*if (op.equals("REDIO_LIST_GET")) {
                ArrayList<String> list = new ArrayList();
                for (i = 0; i < 5; i++) {
                    for (int j = 0; j < 6; j++) {
                        list.add("rate:" + RadioService.this.freq[i][j] + ",name:" + (i * 6) + "" + j);
                    }
                }
                it = new Intent("ACTION_HCT_REDIO_LIST");
                it.putStringArrayListExtra("RADIO_LIST", list);
                RadioService.this.sendBroadcast(it);
            } else */if (op.equals("REDIO_PLAY")) {
                /*if (intent.hasExtra("CHANNEL_RATE")) {
                    String freq = intent.getStringExtra("CHANNEL_RATE");
                    it = new Intent(RadioService.this, RadioActivity.class);
                    it.addFlags(268435456);
                    it.putExtra("freq", freq);
                    RadioService.this.startActivity(it);
                    return;
                }
                it = new Intent(RadioService.this, RadioActivity.class);
                it.addFlags(268435456);
                RadioService.this.startActivity(it);*/
                } else if (op.equals("REDIO_STOP")) {
                    it = new Intent("com.microntek.finish");
                    it.putExtra("class", "com.microntek.radio");
                    _context.sendBroadcast(it);
                }
            }
        }
    };

    @Override
    public void attach(Handler handler, String type) {
        if (_receiver == null) {
            _receiver = _rcvr;
            _context.registerReceiver(_receiver, intentFilter);
        }
        String[] keys = type.split(",");
        for (String key: keys) {
            _handlers.put(key, handler);
        }
    }

    @Override
    public void setParameters(String parameters) {
        if (_audioMgr != null)
            _audioMgr.setParameters(parameters);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public String getParameters(String para) {
        if (_audioMgr != null)
            return _audioMgr.getParameters(para);
        return "";
    }

    @Override
    public void detach() {
        _handlers.clear();
        if (_receiver !=null) {
            _context.unregisterReceiver(_receiver);
            _receiver = null;
        }
    }
}
