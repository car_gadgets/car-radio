package com.bulleratz.bradio;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import greenmesh.ui.Theme;

/**
 * Created by maucher on 15.11.2017.
 */

public class WhiteRadioStationButton extends RadioStationButton {
    ImageView _button = null;
    Drawable _drawable = null;
    RelativeLayout _layout = null;
    boolean _selected = false;
    TextView _txtStationName = null;
    Theme _theme = null;
    ImageView _ivBorder = null;

    private Theme.ThemeObserver _themeObserver = new Theme.ThemeObserver() {
        @Override
        public void colorChanged(Theme theme, String ColorId) {
            setSelected(_selected);
        }

        @Override
        public void drawableChanged(String drawableId, Drawable drawable) {

        }


    };

    public WhiteRadioStationButton(Context context) {
        super(context);
        init();
    }

    public WhiteRadioStationButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WhiteRadioStationButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void init()
    {
        inflate(getContext(), R.layout.white_station_button_layout, this);
        _button = (ImageView)findViewById(R.id.btnStationButton);
        _layout = (RelativeLayout)findViewById(R.id.buttonLayout);
        _txtStationName = (TextView)findViewById(R.id.txtStationText);
        _ivBorder = (ImageView)findViewById(R.id.ivBorder);
        _theme = CurrentTheme.getInstance();
        _theme.addObserver(_themeObserver);
        setSelected(_selected);

        _button.setImageDrawable(_drawable);
    }

    protected void setText(String text) {
        _txtStationName.setText(text);
    }

    protected void setImage(Drawable image) {
        _drawable = image;
        if (_button != null)
            _button.setImageDrawable(_drawable);
    }

    public void setSelected(boolean selected) {
        if (_ivBorder != null && _theme != null) {
            _selected = selected;
            Drawable drawable = _ivBorder.getDrawable();
            GradientDrawable gr = (GradientDrawable)drawable;
                if (selected) {

                    gr.setStroke(3, _theme.getColor(CurrentTheme.THEME_COLOR_SELECTED));
                    gr.setColor(_theme.getColor(CurrentTheme.THEME_COLOR_STATION_BUTTON_SELECTED));
                } else {
                    gr.setStroke(3, _theme.getColor(CurrentTheme.THEME_COLOR_NOT_SELECTED));
                    gr.setColor(_theme.getColor(CurrentTheme.THEME_COLOR_STATION_BUTTON));
                }

        }
    }
}
