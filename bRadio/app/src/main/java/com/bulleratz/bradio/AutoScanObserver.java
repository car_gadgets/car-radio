package com.bulleratz.bradio;

public interface AutoScanObserver {
    void scanStarted(RadioStationBouquet bouquet);
    void stationFound(RadioStation station);
    void scanFinished();
    void progress(int minFreq, int maxFreq, int freq);
}
