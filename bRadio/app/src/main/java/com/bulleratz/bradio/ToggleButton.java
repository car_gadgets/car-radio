package com.bulleratz.bradio;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import greenmesh.ui.Theme;

/**
 * Created by maucher on 12.10.2017.
 */

public class ToggleButton extends RelativeLayout {
    private ImageView _ivLedIndicator = null;
    private ImageView _ivBackground = null;
    private boolean _isChecked = false;
    private Button.OnClickListener _onClickListener = null;
    private int _imageResource = 0;
    private TextView _tvText = null;
    private String _text = "";
    private boolean _isCheckable = true;
    private ImageView _ivButtonImage = null;
    private float _textSize = -1;
    private Theme _theme = null;

    private Theme.ThemeObserver _themeObserver = new Theme.ThemeObserver() {
        @Override
        public void colorChanged(Theme theme, String ColorId) {
            updateLed();
        }


        @Override
        public void drawableChanged(String drawableId, Drawable drawable) {

        }
    };

    public ToggleButton(Context context) {
        super(context);
        init();
    }

    public ToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadAttributes(context, attrs);
        init();
    }

    public ToggleButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadAttributes(context, attrs);
        init();
    }

    void loadAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ToggleButton,
                0, 0);

        try {
            _text = a.getString(R.styleable.ToggleButton_text);
            _isCheckable = a.getBoolean(R.styleable.ToggleButton_checkable, true);
            _isChecked = a.getBoolean(R.styleable.ToggleButton_checked, false);
            _imageResource = a.getResourceId(R.styleable.ToggleButton_src, 0);
            _textSize = a.getDimension(R.styleable.ToggleButton_textSize, -1);
        } finally {
            a.recycle();
        }
    }

    public void setOnClickListener(Button.OnClickListener listener) {
        _onClickListener = listener;
    }

    public void setChecked(boolean checked) {
        if (!isCheckable())
            checked = false;
        if (_isChecked != checked) {
            _isChecked = checked;
            updateLed();
        }

    }

    private void updateLed() {
        if (_ivLedIndicator != null && _theme != null)
            if (_isChecked)
                _ivLedIndicator.setColorFilter(_theme.getColor(CurrentTheme.THEME_COLOR_SELECTED));
            else
                _ivLedIndicator.setColorFilter(_theme.getColor(CurrentTheme.THEME_COLOR_NOT_SELECTED));
    }

    public boolean isCheckable() {
        return _isCheckable;
    }

    public void serCheckable(boolean checkable) {
        _isCheckable = checkable;
        if (!_isCheckable && _isChecked)
            setChecked(false);
    }

    public boolean isChecked() {
        return _isChecked;
    }

    public void setText(String text) {
        _text = text;
        if (_tvText != null)
            _tvText.setText(text);
    }

    public String getText() {
        return _text;
    }

    void init()
    {
        inflate(getContext(), R.layout.toggle_button_layout, this);
        _ivLedIndicator = (ImageView)findViewById(R.id.ivLedIndicator);
        _ivBackground = (ImageView)findViewById(R.id.ivToggleButtonBackground);
        _tvText = (TextView)findViewById(R.id.tvToggleButtonText);
        _ivButtonImage = (ImageView)findViewById(R.id.ivToggleButtonImage);

        _tvText.setText(_text);
        _theme = CurrentTheme.getInstance();
        _theme.addObserver(_themeObserver);

        if (_ivLedIndicator != null) {
            updateLed();
        }

        if (_ivBackground != null) {
            _ivBackground.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    setChecked(!isChecked());
                    if (_onClickListener != null)
                        _onClickListener.onClick(ToggleButton.this);
                }
            });
        }
        if (_ivButtonImage != null && _imageResource != 0) {
            _ivButtonImage.setImageResource(_imageResource);
        }
        if (_tvText != null && _textSize >= 0) {
            _tvText.setTextSize(_textSize);
        }
    }
}
