package com.bulleratz.bradio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

public class BouqetEditorPanel extends Fragment {
    private RadioStationBouquet _bouquet = null;
    private ImageButton _btnAdd = null;
    private ImageButton _btnRemove = null;
    private ImageButton _btnUp = null;
    private ImageButton _btnDown = null;
    private ListView _stationsView = null;
    private Spinner _bouqetSpinner = null;
    private BouqetAdapter _adapter = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bouquet_editor_fragment, null);
        _btnAdd = (ImageButton)view.findViewById(R.id.bouqetEditorButtonAdd);
        _btnRemove = (ImageButton)view.findViewById(R.id.bouqetEditorButtonRemove);
        _btnUp = (ImageButton)view.findViewById(R.id.bouqetEditorButtonUp);
        _btnDown = (ImageButton)view.findViewById(R.id.bouqetEditorButtonDown);
        _stationsView = (ListView)view.findViewById(R.id.bouqet_editor_listview);
        _bouqetSpinner = (Spinner)view.findViewById(R.id.bouquetSpinner);

        _bouqetSpinner.setAdapter(new BouqetManagerAdapter(view.getContext(), RadioService.getBouquetManager()));

        _bouqetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _bouquet = RadioService.getBouquetManager().getBouquet(i);
                _adapter = new BouqetAdapter(view.getContext(), _bouquet);

                _stationsView.setAdapter(_adapter);
                _stationsView.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        _bouquet = RadioService.getBouquetManager().getSelectedBouquet();
        _adapter = new BouqetAdapter(view.getContext(), _bouquet);

        _stationsView.setAdapter(_adapter);
        //_stationsView.setSelection(0);

        _stationsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (_adapter != null) {
                    _adapter.setSelected(_adapter.getItem(i));
                    setSelectedStationVisible();
                }
            }
        });
        //_stationsView.setSelection(1);


        _btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_bouquet != null) {
                    _bouquet.add(new RadioStation());
                    RadioService.getBouquetManager().saveBouquets();
                }
            }
        });



        _btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_bouquet != null && _adapter != null && _bouquet.getCount() > 1) {
                    for (int i=0; i<_bouquet.getCount(); i++) {
                        if (_bouquet.getStation(i) == _adapter.getSelectedStation()) {
                            _bouquet.remove(i);
                            i--;
                            RadioService.getBouquetManager().saveBouquets();
                        }
                    }
                }
            }
        });

        _btnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_adapter != null && _adapter.getSelectedStation() != null) {
                    _bouquet.moveDown(_adapter.getSelectedStation());
                    RadioService.getBouquetManager().saveBouquets();
                }
                setSelectedStationVisible();
            }
        });

        _btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_adapter != null && _adapter.getSelectedStation() != null) {
                    _bouquet.moveUp(_adapter.getSelectedStation());
                    RadioService.getBouquetManager().saveBouquets();
                }
                setSelectedStationVisible();
            }
        });

        return view;
    }

    private void setSelectedStationVisible() {
        for (int i=0; i<_bouquet.getCount(); i++) {
            if (_bouquet.getStation(i) == _adapter.getSelectedStation()) {
                if (_stationsView.getFirstVisiblePosition() > (i - 1) || _stationsView.getLastVisiblePosition() < (i + 1))
                    _stationsView.smoothScrollToPosition(i);
            }
        }
    }

    public RadioStationBouquet getBouqet() {
        return _bouquet;
    }

    public RadioStation getSelectedStation() {
        if (_adapter != null) {
            return _adapter.getSelectedStation();
        }
        return null;
    }
}
