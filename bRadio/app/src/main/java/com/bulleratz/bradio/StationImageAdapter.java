package com.bulleratz.bradio;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hebbe on 20.06.18.
 */

public class StationImageAdapter extends ArrayAdapter<String> {

    private Context _context = null;
    private String _path = "";
    ArrayList<String> _images = new ArrayList<String>();

    private class ViewHolder {
        public ImageView imageView = null;
        public TextView textView = null;
    }

    public StationImageAdapter(Context context, String path) {
        super (context, R.layout.staion_image_item_layout);
        _context = context;
        _path = path;
        _images.add(0, context.getResources().getString(R.string.none_item));
        getImages(path);

    }
    @Override
    public int getCount() {
        int size = _images.size();
        return size;
    }

    @Override
    public String getItem(int i) {
        if (i==0)
            return "";
        return _images.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            view = LayoutInflater.from(_context).inflate(R.layout.staion_image_item_layout, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView)view.findViewById(R.id.tvStationImageItemText);
            viewHolder.imageView = (ImageView)view.findViewById(R.id.ivStationImageItemImage);
            view.setTag(viewHolder);
        }
        viewHolder = (ViewHolder)view.getTag();
        try {
            Drawable drawable = null;
            if (i == 0) {
                drawable = _context.getDrawable(R.drawable.station_default);
            }
            else {
                drawable = Drawable.createFromPath(_images.get(i));
            }
            viewHolder.imageView.setImageDrawable(drawable);
        } catch (Exception e) {

        }
        //viewHolder.imageView.setImageDrawable();
        viewHolder.textView.setText(_images.get(i).replace(_path, ""));
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    private void getImages(String path)
    {
        File file = new File(path);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].getName().toUpperCase().endsWith(".PNG")) {
                        _images.add(files[i].getAbsolutePath());
                    }
                }
            }
        }
    }
}
