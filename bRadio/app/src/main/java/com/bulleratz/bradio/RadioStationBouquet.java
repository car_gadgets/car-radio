package com.bulleratz.bradio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by maucher on 18.09.2017.
 */

public class RadioStationBouquet implements Serializable {

    public interface RadioStationBouqetObserver {
        void stationAdded(int index, RadioStation station);
        void stationRemoved(int index, RadioStation station);
        void nameChanged(String name);
    }

    private transient ArrayList<WeakReference<RadioStationBouqetObserver>> _listeners = new ArrayList<>();

    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        _listeners = new ArrayList<>();
    }

    public void addObserver(RadioStationBouqetObserver observer) {
        for (int i=0; i<_listeners.size(); i++) {
            RadioStationBouqetObserver o = _listeners.get(i).get();
            if (o != null && o == observer)
                return;
            if (o == null) {
                _listeners.remove(i);
                i--;
            }
        }
        _listeners.add(new WeakReference<RadioStationBouqetObserver>(observer));
    }

    public void removeObserver(RadioStationBouqetObserver observer) {
        for (int i=0; i<_listeners.size(); i++) {
            RadioStationBouqetObserver o = _listeners.get(i).get();
            if (o == null || o == observer)
            {
                _listeners.remove(i);
                i--;
            }
        }
    }

    private void notifyObservers(int index, RadioStation station, boolean added) {
        for (int i=0; i<_listeners.size(); i++) {
            RadioStationBouqetObserver o = _listeners.get(i).get();
            if (o != null) {
                if (added)
                    o.stationAdded(index, station);
                else
                    o.stationRemoved(index, station);
            }
            else if (o == null) {
                _listeners.remove(i);
                i--;
            }
        }

    }

    private void notifyNameChanged() {
        for (int i=0; i<_listeners.size(); i++) {
            RadioStationBouqetObserver o = _listeners.get(i).get();
            if (o != null) {
                o.nameChanged(_name);
            }
            else if (o == null) {
                _listeners.remove(i);
                i--;
            }
        }

    }


    ArrayList<RadioStation> _stations = new ArrayList<RadioStation>();

    public void add(RadioStation station)
    {
        add(_stations.size(), station);
    }
    private String _name = "";

    public void remove(int index)
    {
        if (index >= 0 && index <_stations.size()) {
            RadioStation station = _stations.get(index);
            _stations.remove(index);
            notifyObservers(index, station, false);
        }
    }

    public int getCount()
    {
        return _stations.size();
    }

    public RadioStation getStation(int index)
    {
        if (index >= 0 && index <_stations.size())
            return _stations.get(index);
        return null; //TODO: Default freq
    }

    public int getIndexOf(RadioStation station) {
        for (int i=0; i< _stations.size(); i++) {
            if (_stations.get(i) == station)
                return i;
        }
        return -1;
    }

    public String getName() {
        if (_name == null || _name.trim().isEmpty())
            return "Unnamed";
        return _name;
    }

    public void setName(String name) {
        if (name == null)
            name = "";
        if (!name.equals(_name)) {
            _name = name;
            notifyNameChanged();
        }
    }

    public void add(int index, RadioStation station) {
        if (index < 0)
            index = 0;
        if (index > _stations.size())
            index = _stations.size();
        RadioStation st = station.clone();
        _stations.add(index, st);
        notifyObservers(index, st, true);
    }

    public void moveUp(RadioStation station) {
        int index = getIndexOf(station);
        if (index > 0) {
           RadioStation temp = getStation(index -  1);
           _stations.set(index - 1, station);
           _stations.set(index, temp);
           notifyObservers(index - 1, station, true); /* TODO: moved, not added */
        }
    }

    public void moveDown(RadioStation station) {
        int index = getIndexOf(station);
        if (index >= 0 && index < _stations.size() - 1) {
            RadioStation temp = getStation(index +  1);
            _stations.set(index + 1, station);
            _stations.set(index, temp);
            notifyObservers(index + 1, station, true); /* TODO: moved, not added */
        }
    }

    public String toXml() {
        String out = "<Bouquet name=\"" + _name + "\">";
        for (int i=0; i<_stations.size(); i++) {
            out += _stations.get(i).toXml();
        }
        out += "</Bouquet>";
        return out;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject bouquetObject = new JSONObject();
        bouquetObject.put("Name", _name);
        JSONArray stationsArray = new JSONArray();
        for (RadioStation station : _stations) {
            stationsArray.put(station.toJson());
        }
        bouquetObject.put("Stations", stationsArray);
        return bouquetObject;
    }

    public static RadioStationBouquet fromJson(JSONObject object) {
        RadioStationBouquet bouquet = new RadioStationBouquet();
        try {
            bouquet._name = object.getString("Name");
            JSONArray stations = object.getJSONArray("Stations");
            for (int i=0; i<stations.length(); i++) {
                bouquet._stations.add(RadioStation.fromJson(stations.getJSONObject(i)));
            }
            return bouquet;
        }
        catch (Exception exc) {
            return null;
        }
    }
}
