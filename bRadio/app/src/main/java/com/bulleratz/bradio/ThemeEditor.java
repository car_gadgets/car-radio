package com.bulleratz.bradio;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import greenmesh.ui.Theme;

public class ThemeEditor extends Activity {

    private Spinner _spTheme = null;
    private ImageButton _btnSelected = null;
    private ImageButton _btnNotSelected = null;
    private ImageButton _btnAccent = null;
    private Theme currentTheme = null;
    private Button _btnOk = null;
    private Button _btnCancel = null;
    private ImageButton _btnWallpaper = null;
    private Button _btnSaveAs = null;

    private void showColorPickerDialog(final String colorId) {
        ColorPickerDialogBuilder
                .with(ThemeEditor.this)
                .initialColor(currentTheme.getColor(colorId))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .showBorder(false)
                .density(8)
                .setPositiveButton(getResources().getString(R.string.ok), new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        currentTheme.setColor(colorId, selectedColor);
                        updateDisplay();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    static int PICK_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UiSettings.init(getApplicationContext());
        Settings.restoreSettings(getApplicationContext());

        setContentView(R.layout.activity_theme_dialog);

        currentTheme = new Theme();
        currentTheme.copy(CurrentTheme.getInstance());

        _spTheme = (Spinner)findViewById(R.id.sp_theme);
        _btnAccent = (ImageButton) findViewById(R.id.btn_accent);
        _btnSelected = (ImageButton) findViewById(R.id.btn_selected);
        _btnNotSelected = (ImageButton) findViewById(R.id.btn_not_selected);
        _btnOk = (Button)findViewById(R.id.btn_ok);
        _btnCancel = (Button)findViewById(R.id.btn_cancel);
        _btnWallpaper = (ImageButton)findViewById(R.id.btn_wallpaper);
        _btnSaveAs = (Button)findViewById(R.id.btn_save_as);

        _btnAccent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showColorPickerDialog(CurrentTheme.THEME_COLOR_ACCENT);
            }
        });

        _btnNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showColorPickerDialog(CurrentTheme.THEME_COLOR_NOT_SELECTED);
            }
        });

        _btnSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showColorPickerDialog(CurrentTheme.THEME_COLOR_SELECTED);
            }
        });
        _btnWallpaper.setOnClickListener(new View.OnClickListener() {
            int clickCount = 0;
            Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (clickCount == 1) {
                        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        getIntent.setType("image/*");

                        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pickIntent.setType("image/*");

                        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                        //chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {getIntent});

                        startActivityForResult(chooserIntent, PICK_IMAGE);
                        clickCount = 0;
                    }
                }
            };
            @Override
            public void onClick(View view) {
                clickCount++;
                if (clickCount == 1) {
                    handler.sendEmptyMessageDelayed(0, 300);
                } else {
                    handler.removeMessages(0);
                    currentTheme.setDrawable(CurrentTheme.THEME_DRAWABLE_WALLPAPER, null);
                    clickCount = 0;
                    updateDisplay();
                }
            }

        });


        _btnSaveAs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ThemeEditor.this);
                builder.setTitle(getResources().getString(R.string.wallpaper_name));

// Set up the input
                final EditText input = new EditText(ThemeEditor.this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                builder.setView(input);

// Set up the buttons
                builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString();
                        if (!text.isEmpty() && !text.toUpperCase().endsWith(".JSON")) {
                            text = text + ".json";
                        }
                        if (!text.isEmpty()) {
                            currentTheme.save("/sdcard/bradio/themes/" + text);
                        }
                    }
                });
                builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        _spTheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CurrentTheme.resetToDefault(currentTheme);
                try {
                    if (i == 0) {

                    } else if (i==1) {
                        currentTheme.copy(CurrentTheme.getInstance());
                    }
                    else {
                        currentTheme.load(Constants.THEME_STORAGE_PATH + _spTheme.getSelectedItem().toString());
                    }
                    updateDisplay();
                } catch (Exception exc) {
                    _spTheme.setSelection(0);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        initThemeAdapter();

        _btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CurrentTheme.getInstance().copy(currentTheme);
                finish();
            }
        });

        _btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PICK_IMAGE) {
            try {
                if (data == null) {
                    //Display an error
                    return;
                }
                InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                Drawable bmp = Drawable.createFromStream(inputStream, null);
                currentTheme.setDrawable(CurrentTheme.THEME_DRAWABLE_WALLPAPER, bmp);
                updateDisplay();

            } catch (Exception exc) {

            }
        }
    }

    private void updateDisplay() {
        _btnAccent.setColorFilter(currentTheme.getColor(CurrentTheme.THEME_COLOR_ACCENT));
        _btnSelected.setColorFilter(currentTheme.getColor(CurrentTheme.THEME_COLOR_SELECTED));
        _btnNotSelected.setColorFilter(currentTheme.getColor(CurrentTheme.THEME_COLOR_NOT_SELECTED));
        _btnWallpaper.setImageDrawable(currentTheme.getDrawable(CurrentTheme.THEME_DRAWABLE_WALLPAPER));
    }

    private void initThemeAdapter() {
        File file = new File(Constants.THEME_STORAGE_PATH);
        ArrayList<String> themeFiles = new ArrayList<>();
        themeFiles.add(getResources().getString(R.string.none_item));
        themeFiles.add("<current>");
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();
            for (int i=0; i<files.length; i++) {
                if (files[i].getName().toUpperCase().endsWith(".JSON")) {
                    themeFiles.add(files[i].getName());
                }
            }
        }

        String[] items = new String[themeFiles.size()];
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, themeFiles.toArray(items));
        _spTheme.setAdapter(adapter);
        _spTheme.setSelection(1);
    }
}
