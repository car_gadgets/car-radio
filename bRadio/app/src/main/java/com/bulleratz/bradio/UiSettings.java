package com.bulleratz.bradio;

import android.content.Context;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import greenmesh.settings.BoolCheckboxSettingEditor;
import greenmesh.settings.BoolSettingsHandler;
import greenmesh.settings.DropDownSettingsEditor;
import greenmesh.settings.InfoBarSettingHandler;
import greenmesh.settings.InfoBarSettingsEditor;
import greenmesh.settings.IntSettingsHandler;
import greenmesh.settings.SettingsRepository;

public class UiSettings {
    public static final String FREQ_BAR_VISIBLE = "FreqBarVisible";
    public static final String FULLSCREEN = "Fullscreen";
    public static final String FONT_SCALE = "FontScale";
    public static final String BACK_BUTTON_ACION = "BackButtonAction";
    public static final String TOP_LEFT_INFO_BAR_ITEMS = "TopLeftInfoBarItems";
    public static final String TOP_RIGHT_INFO_BAR_ITEMS = "TopRightInfoBarItems";
    public static final String TOP_CENTER_INFO_BAR_ITEMS = "TopCenterInfoBarItems";
    public static final String BOTTOM_CENTER_INFO_BAR_ITEMS = "BottomCenterInfoBarItems";

    public static final int BACK_BUTTON_ACTION_NOTIFY = 2;
    public static final int BACK_BUTTON_ACTION_EXIT = 0;
    public static final int BACK_BUTTON_ACTION_HIDE = 1;

    private static SettingsRepository _settingsRepo = new SettingsRepository();
    private static BoolSettingsHandler _freqBarVisible = new BoolSettingsHandler(FREQ_BAR_VISIBLE);
    private static BoolSettingsHandler _fullscreen = new BoolSettingsHandler(FULLSCREEN);
    private static IntSettingsHandler _fontScale = new IntSettingsHandler(FONT_SCALE);
    private static IntSettingsHandler _backButtonAction = new IntSettingsHandler(BACK_BUTTON_ACION);
    private static InfoBarSettingHandler _topLeftInfoBarItems = new InfoBarSettingHandler(TOP_LEFT_INFO_BAR_ITEMS);
    private static InfoBarSettingHandler _topRightInfoBarItems = new InfoBarSettingHandler(TOP_RIGHT_INFO_BAR_ITEMS);
    private static InfoBarSettingHandler _topCenterInfoBarItems = new InfoBarSettingHandler(TOP_CENTER_INFO_BAR_ITEMS);
    private static InfoBarSettingHandler _bottomCenterInfoBarItems = new InfoBarSettingHandler(BOTTOM_CENTER_INFO_BAR_ITEMS);
    private static Context _context = null;

    private static final SettingsRepository.SettingsRepositoryObserver _repoObserver = new SettingsRepository.SettingsRepositoryObserver() {
        @Override
        public void valueChanged(String name, Object value) {
            if (_context != null) {
                UiSettings.save(getPath(_context));
            }
        }
    };

    public static void addObserver(SettingsRepository.SettingsRepositoryObserver observer) {
        _settingsRepo.addObserver(observer);
    }

    public static void removeObserver(SettingsRepository.SettingsRepositoryObserver observer) {
        _settingsRepo.removeObserver(observer);
    }

    static {
        _freqBarVisible.setValue(false);
        _freqBarVisible.setDisplayName("Show Frequency Bar");
        _fullscreen.setValue(false);
        _fullscreen.setDisplayName("Fullscreen On");
        _fontScale.setValue(10);
        _fontScale.setDisplayName("Font Scale Facor");
        _backButtonAction.setValue(BACK_BUTTON_ACTION_NOTIFY);
        _topLeftInfoBarItems.add(InfoBar.TA_VISIBLE | InfoBar.VISIBLE_FLAG);
        _topLeftInfoBarItems.add(InfoBar.TP_VISIBLE | InfoBar.VISIBLE_FLAG);
        _topLeftInfoBarItems.add(InfoBar.ST_VISIBLE  | InfoBar.VISIBLE_FLAG);
        _topLeftInfoBarItems.onLoaded();
        _topCenterInfoBarItems.add(InfoBar.CLOCK_VISIABLE  | InfoBar.VISIBLE_FLAG);
        _topCenterInfoBarItems.onLoaded();
        _topRightInfoBarItems.add(InfoBar.BOUQUET_SELECTION_VISIBLE | InfoBar.VISIBLE_FLAG);
        _topRightInfoBarItems.onLoaded();
        _bottomCenterInfoBarItems.onLoaded();


        _settingsRepo.addObserver(_repoObserver);
        _settingsRepo.addHandler(_freqBarVisible);
        _settingsRepo.addHandler(_fullscreen);
        _settingsRepo.addHandler(_fontScale);
        _settingsRepo.addHandler(_backButtonAction);
        _settingsRepo.addHandler(_topLeftInfoBarItems);
        _settingsRepo.addHandler(_topCenterInfoBarItems);
        _settingsRepo.addHandler(_topRightInfoBarItems);
        _settingsRepo.addHandler(_bottomCenterInfoBarItems);
    }

    public static void init(Context context) {
        if (_context == null) {
            _context = context;
            if (context != null) {
                load(getPath(context));
            }
        }
    }

    private static String getPath(Context context) {
        return context.getFilesDir() + "/UiSettings.json";
    }

    private static void load(String filename) {
        BufferedReader reader = null;
        File file = new File(filename);
        if (file.exists()) {
            try {
                reader = new BufferedReader(new FileReader(file));
                StringBuilder builder = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONObject root = new JSONObject(builder.toString());
                _settingsRepo.fromJson(root);


            } catch (Exception exc) {

            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception exc) {}
                }
            }
        }
    }

    private static void save(String filename) {
        FileWriter writer = null;
        try {
            File file = new File(filename);
            File parent = new File(file.getParent());
            parent.mkdirs();

            JSONObject root = _settingsRepo.toJson();

            writer = new FileWriter(filename);
            writer.write(root.toString());
            writer.close();

        } catch (Exception exc) {

        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (Exception exc) {}
            }
        }
    }

    public static boolean getFullscreen() {
        return _fullscreen.getBoolValue();
    }

    public static void setFullscreen(boolean fullscreen) {
        _fullscreen.setValue(fullscreen);
    }

    public static boolean getFreqBarVisible() {
        return _freqBarVisible.getBoolValue();
    }
    public static void setFreqBarVisible(boolean visible) {
        _freqBarVisible.setValue(visible);
    }

    public static float getFontScale() {
        return _fontScale.getIntValue() / 10.0f;
    }

    public static int getBackButtonAction() { return _backButtonAction.getIntValue(); }
    public static void setBackButtonAction(int action) {
        if (action < 0 || action > 2) {
            action = 0;
        }
        _backButtonAction.setValue(action);
    }

    public static void setFontScale(float scale) {
        if (scale <= 0.1) {
            scale = 0.1f;
        }
        if (scale > 2) {
            scale = 2;
        }
        _fontScale.setValue((int)(scale * 10));
    }

    public static List<Integer> getTopLeftInfoBarItems() {
        return _topLeftInfoBarItems.getValues();
    }

    public static List<Integer> getTopCenterBarItems() {
        return _topCenterInfoBarItems.getValues();
    }

    public static List<Integer> getTopRightBarItems() {
        return _topRightInfoBarItems.getValues();
    }

    public static List<Integer> getBottomCenterBarItems() {
        return _bottomCenterInfoBarItems.getValues();
    }

    public static SettingsFragment getSettingsFragment(Context context) {
        SettingsFragment fragment = new SettingsFragment();
        fragment.addSettingsEditor(new BoolCheckboxSettingEditor(context, _freqBarVisible), "UI Settings");
        fragment.addSettingsEditor(new BoolCheckboxSettingEditor(context, _fullscreen), "UI Settings");
        InfoBarSettingsEditor editor = new InfoBarSettingsEditor(context, _topLeftInfoBarItems);
        fragment.addSettingsEditor(editor, "UI Settings");
        editor = new InfoBarSettingsEditor(context, _topCenterInfoBarItems);
        fragment.addSettingsEditor(editor, "UI Settings");
        editor = new InfoBarSettingsEditor(context, _topRightInfoBarItems);
        fragment.addSettingsEditor(editor, "UI Settings");
        editor = new InfoBarSettingsEditor(context, _bottomCenterInfoBarItems);
        fragment.addSettingsEditor(editor, "UI Settings");
        DropDownSettingsEditor dropDownEditor = new DropDownSettingsEditor(context, _backButtonAction);
        fragment.addSettingsEditor(dropDownEditor , "UI Settings");
        dropDownEditor.addEnumValue(BACK_BUTTON_ACTION_EXIT, "Exit");
        dropDownEditor.addEnumValue(BACK_BUTTON_ACTION_NOTIFY, "Notify");
        dropDownEditor.addEnumValue(BACK_BUTTON_ACTION_HIDE, "Hide");
        return fragment;

    };

    public static String getTheme() {
        if (_context != null) {
            return _context.getFilesDir() + "/theme.json";
        }
        return "theme.json";
    }

    public static InfoBarSettingHandler getTopLeftSettingsHandler() {
        return _topLeftInfoBarItems;
    }

    public static InfoBarSettingHandler getTopCenterSettingsHandler() {
        return _topCenterInfoBarItems;
    }

    public static InfoBarSettingHandler getTopRightSettingsHandler() {
        return _topRightInfoBarItems;
    }

    public static InfoBarSettingHandler getBottomCenterSettingsHandler() {
        return _bottomCenterInfoBarItems;
    }
}
