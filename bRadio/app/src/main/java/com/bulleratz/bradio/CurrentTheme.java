package com.bulleratz.bradio;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import greenmesh.ui.Colors;
import greenmesh.ui.Theme;

public class CurrentTheme extends Theme {
    private static CurrentTheme _currentTheme = null;

    private CurrentTheme()
    {}

    public static synchronized CurrentTheme getInstance() {
        if (_currentTheme == null) {
            _currentTheme = new CurrentTheme();
            _currentTheme.load(UiSettings.getTheme());
        }
        return _currentTheme;
    }

    public static void resetToDefault(Theme theme) {
        reset(theme);
    }

    private static void reset(Theme theme) {
        theme.setColor(THEME_COLOR_SELECTED, Color.argb(0xff, 0xff, 0x00, 0x00));
        theme.setColor(THEME_COLOR_NOT_SELECTED, Color.argb(0xff, 0x88, 0x00, 0x00));
        theme.setColor(THEME_COLOR_ACCENT, 0xffff0000);
        theme.setColor(THEME_COLOR_BACKGROUND_LIGHT, 0xff353535);
        theme.setColor(THEME_COLOR_BACKGROUND, 0xff101010);
        theme.setColor(THEME_COLOR_STATION_BUTTON, 0xffffffff);
        theme.setColor(THEME_COLOR_STATION_BUTTON_SELECTED , 0xffffffff);

        theme.setDrawable(THEME_DRAWABLE_WALLPAPER, null);
    }

    public void reset() {
        reset(this);
    }


    public void setTheme(String filename) {
        load(filename);
        save();
    }

    public void copy(Theme source) {
        super.copy(source);
        save();
    }

    public void save() {
        save(UiSettings.getTheme());
    }

    public static final String THEME_COLOR_SELECTED = "ColorSelected";
    public static final String THEME_COLOR_NOT_SELECTED = "ColorNotSelected";
    public static final String THEME_COLOR_ACCENT = "ColorAccent";
    public static final String THEME_COLOR_BACKGROUND_LIGHT = "ColorBackgroundLight";
    public static final String THEME_COLOR_BACKGROUND = "ColorBackground";
    public static final String THEME_COLOR_STATION_BUTTON = "ColorStationButton";
    public static final String THEME_COLOR_STATION_BUTTON_SELECTED = "ColorStationButtonSelected";

    public static final String THEME_DRAWABLE_WALLPAPER = "Wallpaper";
}
