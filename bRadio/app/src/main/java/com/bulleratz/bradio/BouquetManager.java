package com.bulleratz.bradio;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by maucher on 09.10.2017.
 */

public class BouquetManager {

    ArrayList<RadioStationBouquet> _boquets = null;
    RadioStationBouquet _bouquet = null;
    ArrayList<IBouquetManagerListener> _listeners = new ArrayList<IBouquetManagerListener>();
    int _selectedStationIndex = -1;
    RadioStation _selectedStation = null;
    RadioService _radioService = null;
    RadioStation _noBouquetStation = null;

    private Handler _afHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int frequency = msg.arg1;
            if (_selectedStation != null && _selectedStation.getFrequency() != frequency) {
                if (_selectedStation.matches(frequency)) { // Rds change to known
                    _selectedStation.setCurrentFrequency(frequency);
                } else if (_selectedStation.getPi() != 0 && _radioService.getPi() == _selectedStation.getPi()) { // new rds
                    _selectedStation.setCurrentFrequency(frequency);
                } else {
                    selectStation(-1);
                }
                /*if (_radioService.getPi() != _selectedStation.getPi() || _radioService.getPi() == 0 || _selectedStation.getPi() == 0) {
                    selectStation(-1);
                } else {
                    if (_selectedStation != _noBouquetStation) {
                        _selectedStation.setFrequency(frequency);
                    }
                }*/

            }
            // Manual change
            if (_selectedStation.getFrequency() == frequency) {
            }
            addFrequency(_radioService.getFrequency(), _radioService.getPi());
        }
    };

    private void addFrequency(int frequeny, int pi) {
        if (pi != 0) {
            for (RadioStationBouquet bouquet : _boquets) {
                for (RadioStation station : bouquet._stations) {
                    if (station.getPi() == pi) {
                        station.addFrequency(frequeny);
                    }
                }
            }
        }
    }

    public void updateStation(int index) {

        if (_bouquet != null && index >0 && index < _bouquet.getCount() &&_radioService != null) {
            RadioStation station = _bouquet.getStation(index);
            station.setUserDefinedName("");
            station.setUserDefinedImage(null);
            station.setFrequency(_radioService.getFrequency());
            station.setName(_radioService.getPsn());
            station.resetImage();

            saveBouquets();
            selectStation(index);
        }
    }

    public int getBouquetCount() {
        return _boquets.size();
    }

    public RadioStationBouquet getBouquet(int index) {
        if (index >= 0 && index < _boquets.size())
            return _boquets.get(index);
        return null;
    }

    private RadioStation getNoBoquetStation() {
        if (_noBouquetStation == null)
            _noBouquetStation = new RadioStation();
        return _noBouquetStation;
    }

    private IRadioServiceListener _radioServiceListener = new IRadioServiceListener() {
        @Override
        public void frequencyChanged(int frequency) {
            if (_selectedStation != null && !_selectedStation.matches(frequency)) {
                //Frequency is not the current station
                Message msg = _afHandler.obtainMessage();
                msg.what = 0;
                msg.arg1 = frequency;
                _afHandler.sendMessageDelayed(msg, 100);
            }
        }

        @Override
        public void psnAvailable(String psn) {
            if (_selectedStation != null) {
                //Don't set empty Names for bouquet stations
                if ((psn == null || psn.isEmpty()) && _selectedStation != getNoBoquetStation())
                    return;
                _selectedStation.setName(psn);
                //selectStation(getSelectedStationIndex()); //TODO: Workaround für Button-Update
            }
        }

        @Override
        public void titleAvailable(String title) {
            if (_selectedStation != null)
                _selectedStation.setCurrentTitle(title);
        }

        @Override
        public void ptyChanged(int pty) {

        }

        @Override
        public void piChanged(int pi) {
            if (_selectedStation != null && _selectedStation != getNoBoquetStation() && pi != 0 && _selectedStation.getPi() == 0) {
                _selectedStation.setPi(pi);
            }

            if (_radioService != null)
                addFrequency(_radioService.getFrequency(), pi);
            saveBouquets();
        }

        @Override
        public void stereoChanged(boolean stereo) {

        }

        @Override
        public void strengthChanged(int strength) {

        }

        @Override
        public void seekStart() {

        }

        @Override
        public void autoseekStart() {

        }

        @Override
        public void seekFound() {

        }

        @Override
        public void autoseekFound() {

        }

        @Override
        public void seekEnd() {

        }

        @Override
        public void autoseekEnd(RadioStationBouquet seekResult) {

        }

        @Override
        public void tpChanged(boolean tp) {

        }

        @Override
        public void taChanged(boolean ta) {

        }

        @Override
        public void bandChanged() {

        }
    };

    public void setRadioService(RadioService radioService) {
        if (_radioService != radioService) {
            if (_radioService != null)
                _radioService.removeRadioServiceListener(_radioServiceListener);
            _radioService = radioService;

            if (_radioService != null)
                _radioService.addRadioServiceListener(_radioServiceListener);
        }
    }

    public interface IBouquetManagerListener {
        public void selectedBouquetChanged(RadioStationBouquet bouquet);
        public void selectedStationChanged(RadioStation station, int stationIndex);
        public void bouquetAdded(RadioStationBouquet bouquet);
        public void bouquetRemoved(RadioStationBouquet bouquet);
    }

    public void addBouquetManagerListener(IBouquetManagerListener listener) {
        if (!_listeners.contains(listener)) {
            _listeners.add(listener);
        }
    }

    public void fixSelectedStation() {
        RadioStation station = getSelectedStation();
        if (station != _noBouquetStation)
            station.fix();
        if (getSelectedBouquet() != null)
            saveBouquets();
    }

    public void removeBouquetManagerListener(IBouquetManagerListener listener) {
        _listeners.remove(listener);
    }

    public void selectStation(int stationIndex) {
        if (_radioService == null)
            Log.d("BRadio", "Radio service is NULL");
        RadioStation oldStation = _selectedStation;
        if (/*stationIndex != _selectedStationIndex && *//*TODO: Workarounf für button update*/_bouquet != null && stationIndex >= 0 && stationIndex < _bouquet.getCount()) {
            _selectedStationIndex = stationIndex;
            _selectedStation = _bouquet.getStation(_selectedStationIndex);
            if (_selectedStation != null) {
                if (_radioService != null ) {
                    _radioService.setFrequency(_selectedStation.getFrequency());
                }
            }
            for (IBouquetManagerListener listener : _listeners) {
                listener.selectedStationChanged(_selectedStation, _selectedStationIndex);
            }
        } else if (stationIndex == -1) {
            _selectedStationIndex = stationIndex;
            _selectedStation = getNoBoquetStation();
            _selectedStation.setCurrentTitle("");

            if (_radioService != null) {
                _selectedStation.setName(_radioService.getPsn());
                _selectedStation.setFrequency(_radioService.getFrequency());
                _selectedStation.setPi(0);
            } else {
                _selectedStation.setName("");
                _selectedStation.setFrequency(87500000);
            }
            for (IBouquetManagerListener listener : _listeners) {
                listener.selectedStationChanged(_selectedStation, _selectedStationIndex);
            }
        }
        if (oldStation != _selectedStation && oldStation != null && oldStation != getNoBoquetStation()) {
            oldStation.setCurrentFrequency(0);
        }
    }

    public RadioStation getSelectedStation() {
        return _selectedStation;
    }

    public int getSelectedStationIndex() {
        return _selectedStationIndex;
    }

    public void saveBouquets() {
            saveBouquets(Constants.BOUQUET_FILE);
    }

    public void saveBouquets(String filename) {
        if (filename == null || filename.isEmpty()) {
            saveBouquets();
            return;
        }

        File file = new File(filename);
    /*
        try {
            if (!file.exists()) {
                File dir = file.getParentFile();
                dir.mkdirs();
            }
            FileOutputStream outputStream = new FileOutputStream(file, false);
            ObjectOutputStream os = new ObjectOutputStream(outputStream);
            os.writeObject(_boquets);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            JSONObject root = new JSONObject();
            JSONArray bouquets = new JSONArray();
            for (RadioStationBouquet bouqet : _boquets) {
                bouquets.put(bouqet.toJson());
            }
            root.put("Bouquets", bouquets);

            file = new File(filename + ".json");
            if (!file.exists()) {
                File dir = file.getParentFile();
                dir.mkdirs();
            }
            FileWriter writer = new FileWriter(filename + ".json");
            writer.write(root.toString());
            writer.close();
        } catch (Exception exc) {

        }
    }

    public void loadBouquets() {
        loadBouquets(Constants.BOUQUET_FILE);
    }

    public void loadBouquets(String filename) {
        if (filename == null || filename.isEmpty()) {
            loadBouquets();
            return;
        }

        File file = new File(filename + ".json");
        if (file.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));
                StringBuilder builder = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                JSONObject root = new JSONObject(builder.toString());
                JSONArray bouquets = root.getJSONArray("Bouquets");
                if (_boquets == null) {
                    _boquets = new ArrayList<>();
                }
                for (int i = 0; i < bouquets.length(); i++) {
                    _boquets.add(RadioStationBouquet.fromJson(bouquets.getJSONObject(i)));
                }

            } catch (Exception exc) {

            }
        } else {
            file = new File(filename);
            if (file.exists()) {
                try {

                    FileInputStream inputStream = new FileInputStream(file);
                    ObjectInputStream is = new ObjectInputStream(inputStream);
                    _boquets = (ArrayList<RadioStationBouquet>) is.readObject();
                    is.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        createBouquets();
    }

    private void createBouquets() {
        if (_boquets == null || _boquets.size() == 0) {
            _boquets = new ArrayList<RadioStationBouquet>();
            RadioStationBouquet temp = new RadioStationBouquet();

            RadioStation rs = new RadioStation();

            for (int i = 0; i < 15; i++) {
                rs = new RadioStation();
                rs.setFrequency(87500000);
                temp.add(rs);
            }
            _boquets.add(temp);

            saveBouquets();
        }
        setBouqet(_boquets.get(0));
    }

    public RadioStationBouquet getSelectedBouquet() {
        if (_bouquet == null)
            loadBouquets();
        return _bouquet;
    }

    public RadioStationBouquet selectDefalutBouquet() {
        if (_boquets == null)
            loadBouquets();
        setBouqet(_boquets.get(0));
        return _bouquet;
    }

    public RadioStationBouquet selectBouquet(String name) {
        if (_boquets == null)
            loadBouquets();
        for (RadioStationBouquet bouquet : _boquets) {
            if (bouquet.getName().equals(name)) {
                setBouqet(bouquet);
                return _bouquet;
            }
        }
        return _bouquet;
    }

    public void setSelectedBouqet(RadioStationBouquet bouquet) {
        setBouqet(bouquet);
    }

    private void setBouqet(RadioStationBouquet bouquet) {
        if (_bouquet != bouquet) {
            if (bouquet != null) {
                _bouquet = bouquet;
                for (IBouquetManagerListener listener : _listeners) {
                    listener.selectedBouquetChanged(_bouquet);
                }
            }
        }
    }

    public void next() {
        if (_bouquet != null) {
            int nextStation = -1;
            if (_selectedStationIndex >= 0 && _selectedStationIndex < _bouquet.getCount()) {
                nextStation = _selectedStationIndex + 1;
                if (nextStation >= _bouquet.getCount())
                    nextStation = 0;
            } else {
                nextStation = 0; //TODO: nächste frequenz suchen???
            }
            selectStation(nextStation);
        }
    }

    public void prev() {
        if (_bouquet != null) {
            int nextStation = -1;
            if (_selectedStationIndex >= 0 && _selectedStationIndex < _bouquet.getCount()) {
                nextStation = _selectedStationIndex - 1;
                if (nextStation < 0)
                    nextStation = _bouquet.getCount() - 1;
            } else {
                nextStation = 0; //TODO: nächste frequenz suchen???
            }
            selectStation(nextStation);
        }
    }

    public void updateStation(int index, int frequency, String displayName, String imagePath, int pi) {
        RadioStation station = _bouquet.getStation(index);
        station.setFrequency(frequency);
        station.setUserDefinedName(displayName);
        station.setUserDefinedImage(imagePath);
        station.setPi(pi);
        saveBouquets();
    }

    public RadioStation getStation(int index) {
        if (_bouquet != null) {
            return _bouquet.getStation(index);
        }
        return new RadioStation();
    }

    public RadioStationBouquet addBouqet(String name) {
        RadioStationBouquet bouquet = new RadioStationBouquet();
        bouquet.setName(name);
        _boquets.add(bouquet);
        for (IBouquetManagerListener listener : _listeners) {
            listener.bouquetAdded(bouquet);
        }
        return bouquet;
    }

    public void addBouqet(RadioStationBouquet bouquet) {
        if (bouquet != null && !_boquets.contains(bouquet)) {
            _boquets.add(bouquet);
            for (IBouquetManagerListener listener : _listeners) {
                listener.bouquetAdded(bouquet);
            }
        }
    }

    public void removeBouquet(RadioStationBouquet bouqet) {
        if (bouqet != null) {
            if (_boquets.contains(bouqet) && _boquets.size() > 1) {
                boolean changeBouquet = false;
                if (bouqet == getSelectedBouquet()) {
                    changeBouquet = true;
                }
                _boquets.remove(bouqet);
                if (changeBouquet) {
                    setBouqet(_boquets.get(0));
                }
                for (IBouquetManagerListener listener : _listeners) {
                    listener.bouquetRemoved(bouqet);
                }
            }
        }
    }
}
