package com.bulleratz.bradio;

/**
 * Created by maucher on 06.09.2017.
 */

public interface IRadioServiceListener {
    void frequencyChanged(int frequency);
    void psnAvailable(String psn);
    void titleAvailable(String title);
    void ptyChanged(int pty);
    void piChanged(int pi);
    void stereoChanged(boolean stereo);
    void strengthChanged(int strength);
    void seekStart();
    void autoseekStart();
    void seekFound();
    void autoseekFound();
    void seekEnd();
    void autoseekEnd(RadioStationBouquet seekResult);
    void tpChanged(boolean tp);
    void taChanged(boolean ta);
    void bandChanged();
}
