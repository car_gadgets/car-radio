package com.bulleratz.bradio;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class BoquetManagerEditor extends FragmentActivity {
    private BouqetManagerAdapter _adapter = null;
    private int selectedIndex = -1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bouqet_manager_editor);
        ListView bView = (ListView)findViewById(R.id.bouqet_manager_list_view);

        _adapter = new BouqetManagerAdapter(getBaseContext(), RadioService.getBouquetManager());

        bView.setAdapter(_adapter);
        Button btnAdd = (Button)findViewById(R.id.bouqet_manager_add);
        Button btnRemove = (Button)findViewById(R.id.bouqet_manager_remove);
        Button btnEdit = (Button)findViewById(R.id.bouqet_manager_edit);

        bView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedIndex = i;
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioStationBouquet bouquet = RadioService.getBouquetManager().addBouqet("New");
                bouquet.add(new RadioStation());
                RadioService.getBouquetManager().saveBouquets();
            }
        });


        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioStationBouquet b = _adapter.getBouquet(selectedIndex);
                if (b != null && RadioService.getBouquetManager().getBouquetCount() > 1) {
                    RadioService.getBouquetManager().removeBouquet(b);
                }
                RadioService.getBouquetManager().saveBouquets();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(BoquetManagerEditor.this);

                alert.setTitle("Boquet Name");
                alert.setMessage("Boquet Name");

// Set an EditText view to get user input
                final EditText input = new EditText(BoquetManagerEditor.this);
                RadioStationBouquet b = _adapter.getBouquet(selectedIndex);
                if (b != null) {
                    input.setText(b.getName());
                }
                alert.setView(input);

                alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        RadioStationBouquet b = _adapter.getBouquet(selectedIndex);
                        if (b != null) {
                            b.setName(value);
                        }
                        RadioService.getBouquetManager().saveBouquets();
                    }
                });

                alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        RadioService.getBouquetManager().removeBouquetManagerListener(_adapter);
        super.onDestroy();
    }
}
