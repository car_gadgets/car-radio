package com.bulleratz.bradio;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.bulleratz.bradio.mtc.MtcCarManageCallback;
import com.bulleratz.bradio.mtc.MtcCarService;

/**
 * Created by maucher on 24.10.2017.
 */

public abstract class RadioAbstractionLayer {
    private static RadioAbstractionLayer _radioAbstraction = null;
    public abstract void attach(Handler handler, String type);
    public abstract void setParameters(String parameters);
    public abstract boolean isValid();
    public abstract String getParameters(String para);

    public static synchronized RadioAbstractionLayer getAbstractionLayer(Context context) {
        if (_radioAbstraction == null) {
            _radioAbstraction = new CarManagerRadioAbstraction();
        }
        if (!_radioAbstraction.isValid()) {
            if (context != null)
                _radioAbstraction = new LegacyRadioAbstraction(context);
            else
                _radioAbstraction = new MockRadioAbstraction();
        }
        return _radioAbstraction;
    }

    public abstract void detach();
}

class MockRadioAbstraction extends RadioAbstractionLayer {

    private Handler _handler = null;

    @Override
    public void attach(Handler handler, String type) {
        _handler = handler;
    }

    @Override
    public void setParameters(String parameters) {
        String[] pair = parameters.split("=");
        Bundle bundle = null;
        if (pair.length == 2) {
            if (pair[0].equals("ctl_radio_frequency")) {
                int freq = Integer.parseInt(pair[1]);
                bundle = new Bundle();
                bundle.putString("type", "freq");
                bundle.putInt("value", freq * 1000);
            }
        }
        if (bundle != null && _handler != null) {
            Message msg = new Message();
            msg.setData(bundle);
            msg.obj = "Radio";
            _handler.sendMessage(msg);
        }
    }

    @Override
    public String getParameters(String para) {
        return "";
    }

    @Override
    public boolean isValid() {
        return true;
    }

    public void detach() {}
}

class CarManagerRadioAbstraction extends RadioAbstractionLayer {
    public static final int MSG_ON_STATUS = -1;
    private MtcCarService _carManager = null;
    private Handler _handler = null;
    private String _handlerType = "";

    private MtcCarManageCallback _callback = null;

    public boolean isValid() {
        return _carManager != null;
    }

    public CarManagerRadioAbstraction() {
        try
        {
            _carManager = MtcCarService.getInstance();
        } catch (Exception exc)
        {
            _carManager = null;
        }
    }

    @Override
    public void attach(Handler handler, String type){
        if (_callback == null) {
            _handler = handler;
            _handlerType = type;
            try {
                _callback = new MtcCarManageCallback() {
                    @Override
                    public void onStatusChanged(String str, Bundle bundle) {
                        if (_handlerType.contains(str)) {
                                Message msg = Message.obtain(_handler, MSG_ON_STATUS);
                                msg.obj = str;
                                msg.setData(bundle);
                                _handler.sendMessage(msg);
                        }
                    }
                };
                _carManager.registerCallback(_callback);
            } catch (Exception exc) {
                Log.d("BRadio", exc.getMessage());
            }
        }
    }

    @Override
    public void setParameters(String parameters) {
        try {
            _carManager.setParameters(parameters);
        } catch (RemoteException exc) {

        }
    }

    @Override
    public String getParameters(String para) {
        try {
            return _carManager.getParameters(para);
        } catch (RemoteException exc) {
            return "";
        }
    }

    public void detach() {
        try {
            _carManager.unregisterCallback(_callback);
            _callback = null;
            _handler = null;
            _handlerType = "";
        } catch (RemoteException exc) {

        }
    }
}
