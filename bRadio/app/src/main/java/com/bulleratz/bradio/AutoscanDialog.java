package com.bulleratz.bradio;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AutoscanDialog extends AppCompatActivity {
    private RadioService _service = null;
    private Button _startStopButton = null;
    private ProgressBar pbScanProgress = null;
    private TextView tvFreq = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_scan);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        getWindow().setLayout((int)(displayMetrics.widthPixels - 100), displayMetrics.heightPixels / 2);

        _startStopButton = (Button)findViewById(R.id.btnStartStop);
        pbScanProgress = (ProgressBar)findViewById(R.id.pbScanProgress);
        tvFreq = (TextView)findViewById(R.id.tvFreq);

        if (_service == null) {
            Intent intent = new Intent(this, RadioService.class);
            bindService(intent, _connection, Context.BIND_AUTO_CREATE);
        }

        _startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_service != null) {
                    if (!_service.getAutoscanManager().isScanning()) {
                        RadioStationBouquet bouq  = _service.getAutoscanManager().start(RadioService.BAND_FM);
                        if (bouq != null)
                            RadioService.getBouquetManager().addBouqet(bouq);
                    }
                    else
                        _service.getAutoscanManager().cancel();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (_service != null) {
            if (_service.getAutoscanManager().isScanning()) {
                _service.getAutoscanManager().cancel();
            }
            unbindService(_connection);
        }
        super.onDestroy();
    }

    private AutoScanObserver _observer = new AutoScanObserver() {
        @Override
        public void scanStarted(RadioStationBouquet bouquet) {
            _startStopButton.setText("Stop Scan");
        }

        @Override
        public void stationFound(RadioStation station) {

        }

        @Override
        public void scanFinished() {
            _startStopButton.setText("Start Scan");
        }

        @Override
        public void progress(int minFreq, int maxFreq, int freq) {
            if (tvFreq != null) {
                tvFreq.setText(Double.toString((double)freq / 1000000) + " Mhz");
            }
            double full = maxFreq - minFreq;
            double progress = maxFreq - freq;
            double percentage = (progress / full) * 100;
            if (percentage > 100)
                percentage = 100;
            pbScanProgress.setProgress(100 - (int)percentage);
        }
    };

    private ServiceConnection _connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            _service = ((RadioServiceBinder) iBinder).getService();
            _service.getAutoscanManager().addObserver(_observer);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };
}
