package com.bulleratz.bradio;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by maucher on 21.09.2017.
 */

public abstract class RadioStationButton extends RelativeLayout{

    private RadioStation _station = null;

    public RadioStationButton(Context context) {
        super(context);
    }

    public RadioStationButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RadioStationButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    IRadioStationListener _listener = new IRadioStationListener() {
        @Override
        public void radioStationChanged(RadioStation sender) {
            setText(sender.getDisplayName());
            setImage(sender.getImage());
        }
    };

    public void setRadioStation(RadioStation station) {
        if (station != _station) {
            if (_station != null) {
                _station.removeRadioStationListener(_listener);
            }
            _station = station;
            if (_station != null) {
                _station.addRadioStationListener(_listener);
                _listener.radioStationChanged(_station);
            }
        }
    }

    protected abstract void setText(String text);

    protected abstract void setImage(Drawable image);

    public abstract void setSelected(boolean selected);
}
