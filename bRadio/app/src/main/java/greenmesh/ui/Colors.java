package greenmesh.ui;

import java.util.HashMap;

public class Colors {
    private static HashMap<String, Integer> _colors = new HashMap<>();

    private static boolean _isInitialized = false;

    private static synchronized void initialize() {
        if (!_isInitialized) {
            addColor("AliceBlue", 0xFFF0F8FF);
            addColor("AntiqueWhite", 0xFFFAEBD7);
            addColor("Aqua", 0xFF00FFFF);
            addColor("Aquamarine", 0xFF7FFFD4);
            addColor("Azure", 0xFFF0FFFF);
            addColor("Beige", 0xFFF5F5DC);
            addColor("Bisque", 0xFFFFE4C4);
            addColor("Black", 0xFF000000);
            addColor("BlanchedAlmond", 0xFFFFEBCD);
            addColor("Blue", 0xFF0000FF);
            addColor("BlueViolet", 0xFF8A2BE2);
            addColor("Brown", 0xFFA52A2A);
            addColor("BurlyWood", 0xFFDEB887);
            addColor("CadetBlue", 0xFF5F9EA0);
            addColor("Chartreuse", 0xFF7FFF00);
            addColor("Chocolate", 0xFFD2691E);
            addColor("Coral", 0xFFFF7F50);
            addColor("CornflowerBlue", 0xFF6495ED);
            addColor("Cornsilk", 0xFFFFF8DC);
            addColor("Crimson", 0xFFDC143C);
            addColor("Cyan", 0xFF00FFFF);
            addColor("DarkBlue", 0xFF00008B);
            addColor("DarkCyan", 0xFF008B8B);
            addColor("DarkGoldenRod", 0xFFB8860B);
            addColor("DarkGray", 0xFFA9A9A9);
            addColor("DarkGrey", 0xFFA9A9A9);
            addColor("DarkGreen", 0xFF006400);
            addColor("DarkKhaki", 0xFFBDB76B);
            addColor("DarkMagenta", 0xFF8B008B);
            addColor("DarkOliveGreen", 0xFF556B2F);
            addColor("DarkOrange", 0xFFFF8C00);
            addColor("DarkOrchid", 0xFF9932CC);
            addColor("DarkRed", 0xFF8B0000);
            addColor("DarkSalmon", 0xFFE9967A);
            addColor("DarkSeaGreen", 0xFF8FBC8F);
            addColor("DarkSlateBlue", 0xFF483D8B);
            addColor("DarkSlateGray", 0xFF2F4F4F);
            addColor("DarkSlateGrey", 0xFF2F4F4F);
            addColor("DarkTurquoise", 0xFF00CED1);
            addColor("DarkViolet", 0xFF9400D3);
            addColor("DeepPink", 0xFFFF1493);
            addColor("DeepSkyBlue", 0xFF00BFFF);
            addColor("DimGray", 0xFF696969);
            addColor("DimGrey", 0xFF696969);
            addColor("DodgerBlue", 0xFF1E90FF);
            addColor("FireBrick", 0xFFB22222);
            addColor("FloralWhite", 0xFFFFFAF0);
            addColor("ForestGreen", 0xFF228B22);
            addColor("Fuchsia", 0xFFFF00FF);
            addColor("Gainsboro", 0xFFDCDCDC);
            addColor("GhostWhite", 0xFFF8F8FF);
            addColor("Gold", 0xFFFFD700);
            addColor("GoldenRod", 0xFFDAA520);
            addColor("Gray", 0xFF808080);
            addColor("Grey", 0xFF808080);
            addColor("Green", 0xFF008000);
            addColor("GreenYellow", 0xFFADFF2F);
            addColor("HoneyDew", 0xFFF0FFF0);
            addColor("HotPink", 0xFFFF69B4);
            addColor("IndianRed", 0xFFCD5C5C);
            addColor("Indigo", 0xFF4B0082);
            addColor("Ivory", 0xFFFFFFF0);
            addColor("Khaki", 0xFFF0E68C);
            addColor("Lavender", 0xFFE6E6FA);
            addColor("LavenderBlush", 0xFFFFF0F5);
            addColor("LawnGreen", 0xFF7CFC00);
            addColor("LemonChiffon", 0xFFFFFACD);
            addColor("LightBlue", 0xFFADD8E6);
            addColor("LightCoral", 0xFFF08080);
            addColor("LightCyan", 0xFFE0FFFF);
            addColor("LightGoldenRodYellow", 0xFFFAFAD2);
            addColor("LightGray", 0xFFD3D3D3);
            addColor("LightGrey", 0xFFD3D3D3);
            addColor("LightGreen", 0xFF90EE90);
            addColor("LightPink", 0xFFFFB6C1);
            addColor("LightSalmon", 0xFFFFA07A);
            addColor("LightSeaGreen", 0xFF20B2AA);
            addColor("LightSkyBlue", 0xFF87CEFA);
            addColor("LightSlateGray", 0xFF778899);
            addColor("LightSlateGrey", 0xFF778899);
            addColor("LightSteelBlue", 0xFFB0C4DE);
            addColor("LightYellow", 0xFFFFFFE0);
            addColor("Lime", 0xFF00FF00);
            addColor("LimeGreen", 0xFF32CD32);
            addColor("Linen", 0xFFFAF0E6);
            addColor("Magenta", 0xFFFF00FF);
            addColor("Maroon", 0xFF800000);
            addColor("MediumAquaMarine", 0xFF66CDAA);
            addColor("MediumBlue", 0xFF0000CD);
            addColor("MediumOrchid", 0xFFBA55D3);
            addColor("MediumPurple", 0xFF9370DB);
            addColor("MediumSeaGreen", 0xFF3CB371);
            addColor("MediumSlateBlue", 0xFF7B68EE);
            addColor("MediumSpringGreen", 0xFF00FA9A);
            addColor("MediumTurquoise", 0xFF48D1CC);
            addColor("MediumVioletRed", 0xFFC71585);
            addColor("MidnightBlue", 0xFF191970);
            addColor("MintCream", 0xFFF5FFFA);
            addColor("MistyRose", 0xFFFFE4E1);
            addColor("Moccasin", 0xFFFFE4B5);
            addColor("NavajoWhite", 0xFFFFDEAD);
            addColor("Navy", 0xFF000080);
            addColor("OldLace", 0xFFFDF5E6);
            addColor("Olive", 0xFF808000);
            addColor("OliveDrab", 0xFF6B8E23);
            addColor("Orange", 0xFFFFA500);
            addColor("OrangeRed", 0xFFFF4500);
            addColor("Orchid", 0xFFDA70D6);
            addColor("PaleGoldenRod", 0xFFEEE8AA);
            addColor("PaleGreen", 0xFF98FB98);
            addColor("PaleTurquoise", 0xFFAFEEEE);
            addColor("PaleVioletRed", 0xFFDB7093);
            addColor("PapayaWhip", 0xFFFFEFD5);
            addColor("PeachPuff", 0xFFFFDAB9);
            addColor("Peru", 0xFFCD853F);
            addColor("Pink", 0xFFFFC0CB);
            addColor("Plum", 0xFFDDA0DD);
            addColor("PowderBlue", 0xFFB0E0E6);
            addColor("Purple", 0xFF800080);
            addColor("RebeccaPurple", 0xFF663399);
            addColor("Red", 0xFFFF0000);
            addColor("RosyBrown", 0xFFBC8F8F);
            addColor("RoyalBlue", 0xFF4169E1);
            addColor("SaddleBrown", 0xFF8B4513);
            addColor("Salmon", 0xFFFA8072);
            addColor("SandyBrown", 0xFFF4A460);
            addColor("SeaGreen", 0xFF2E8B57);
            addColor("SeaShell", 0xFFFFF5EE);
            addColor("Sienna", 0xFFA0522D);
            addColor("Silver", 0xFFC0C0C0);
            addColor("SkyBlue", 0xFF87CEEB);
            addColor("SlateBlue", 0xFF6A5ACD);
            addColor("SlateGray", 0xFF708090);
            addColor("SlateGrey", 0xFF708090);
            addColor("Snow", 0xFFFFFAFA);
            addColor("SpringGreen", 0xFF00FF7F);
            addColor("SteelBlue", 0xFF4682B4);
            addColor("Tan", 0xFFD2B48C);
            addColor("Teal", 0xFF008080);
            addColor("Thistle", 0xFFD8BFD8);
            addColor("Tomato", 0xFFFF6347);
            addColor("Turquoise", 0xFF40E0D0);
            addColor("Violet", 0xFFEE82EE);
            addColor("Wheat", 0xFFF5DEB3);
            addColor("White", 0xFFFFFFFF);
            addColor("WhiteSmoke", 0xFFF5F5F5);
            addColor("Yellow", 0xFFFFFF00);
            addColor("YellowGreen", 0xFF9ACD32);
            _isInitialized = true;
        }
    }
    
    private static void addColor(String key, int value) {
        _colors.put(key.trim().toUpperCase(), value);
    }

    public static int getColor(String key, byte alpha) {
        int color = (getColor(key) & 0x00ffffff) | (alpha << 24);
        return color;
    }

    public static int getColor(String key) {
        int color = 0;
        if (!_isInitialized)
            initialize();
        if (_colors.containsKey(key.toUpperCase())) {
            color = _colors.get(key.toUpperCase());
        }
        else {
            try {
                color = (int) Long.parseLong(key, 16);
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
        return color;
    }
}
