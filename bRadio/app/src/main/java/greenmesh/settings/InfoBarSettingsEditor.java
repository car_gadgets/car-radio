package greenmesh.settings;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.bulleratz.bradio.BoquetManagerEditor;
import com.bulleratz.bradio.InfoBarEditorDialog;
import com.bulleratz.bradio.R;

public class InfoBarSettingsEditor extends SettingsEditor {
    private ListView lvItems = null;

    public InfoBarSettingsEditor(Context context, final SettingsHandler handler) {
        super(context, handler);

        inflate(context, R.layout.info_bar_settings_editor, this);
        onInflated();
        Button button = (Button)findViewById(R.id.infoBarEditorButton);
        button.setText(handler.getName());
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), InfoBarEditorDialog.class);
                intent.putExtra("Name", handler.getName());
                getContext().startActivity(intent);

            }
        });
        //lvItems = findViewById(R.id.lvInfoBarItems);
        //InfoBarSettingAdapter adapter = new InfoBarSettingAdapter(context, (InfoBarSettingHandler)handler);
        //lvItems.setAdapter(adapter);
    }

    private void onClick() {

    }
}
