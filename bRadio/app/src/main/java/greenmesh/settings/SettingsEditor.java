package greenmesh.settings;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bulleratz.bradio.R;

public class SettingsEditor extends LinearLayout {
    private SettingsHandler _handler = null;

    public SettingsEditor(Context context, SettingsHandler handler) {
        super(context);
        _handler = handler;
    }

    private void setDisplayName(String name) {
        if (name == null)
            name = "";
        TextView tvDisplayName = (TextView)findViewById(R.id.tvDisplayName);
        if (tvDisplayName != null)
            tvDisplayName.setText(name);
    }

    protected void onInflated() {
        if (_handler != null) {
            setDisplayName(_handler.getDisplayName());
        }
    }
}
