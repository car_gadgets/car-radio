package greenmesh.settings;

import android.content.Context;
import android.widget.SeekBar;

import com.bulleratz.bradio.R;

public class IntSliderSettingsEditor extends SettingsEditor {
    private IntSettingsHandler _handler = null;
    private SeekBar _seekBar = null;

    public IntSliderSettingsEditor(Context context, IntSettingsHandler handler) {
        super(context, handler);
        _handler = handler;
        inflate(context, R.layout.setting_panel_int, this);

        onInflated();
        _seekBar = findViewById(R.id.sbValueBar);
        if (_seekBar !=null) {
            _seekBar.setProgress(handler.getIntValue());
            _seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    _handler.setValue(i);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }
}
