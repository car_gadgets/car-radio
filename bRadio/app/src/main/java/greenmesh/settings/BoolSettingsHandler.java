package greenmesh.settings;

import org.json.JSONException;
import org.json.JSONObject;

public class BoolSettingsHandler extends SettingsHandler {

    public BoolSettingsHandler(String name) {
        super(name);
    }

    @Override
    public void toJsonObject(JSONObject object) throws JSONException {
        object.put(STR_VALUE, getValue());
    }

    @Override
    protected boolean acceptValue(Object value) {
        if (value.getClass().equals(Boolean.class)) {
            return true;
        }
        return false;
    }

    @Override
    protected Object loadValue(JSONObject source) throws JSONException {
        setValue(source.getBoolean(STR_VALUE));
        return getValue();
    }

    public void setValue(boolean value) {
        super.setValue(value);
    }

    public boolean getBoolValue() {
        Boolean bool =  (Boolean)getValue();
        if (bool != null)
            return bool.booleanValue();
        return false;
    }
}
