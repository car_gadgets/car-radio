package greenmesh.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bulleratz.bradio.AutoscanDialog;
import com.bulleratz.bradio.BoquetManagerEditor;
import com.bulleratz.bradio.BouquetEditor;
import com.bulleratz.bradio.MainActivity;
import com.bulleratz.bradio.R;
import com.bulleratz.bradio.Settings;
import com.bulleratz.bradio.SettingsActivity;
import com.bulleratz.bradio.ThemeEditor;
import com.bulleratz.bradio.UiSettings;

import java.util.ArrayList;

public class DropDownSettingsEditor extends SettingsEditor {
    ImageView _imageView = null;
    TextView _textView = null;
    TextView _tvValue = null;
    private IntSettingsHandler _handler = null;

    class EnumValue {
        public int value = 0;
        public String text = "";
    }

    private ArrayList<EnumValue> _values = new ArrayList<>();

    public DropDownSettingsEditor(Context context, IntSettingsHandler handler) {
        super(context, handler);
        inflate(context, R.layout.dopdown_view, this);
        _handler = handler;
        _imageView = findViewById(R.id.ivDropDown);
        _textView = findViewById(R.id.tvText);
        _tvValue = findViewById(R.id.tvValue);
        _imageView.setClickable(false);
        _tvValue.setClickable(false);
        _textView.setText(handler.getName());
        _tvValue.setText(getValue(handler.getIntValue()).text);
        setClickable(true);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showDropDown();
            }
        });
        handler.addObserver(new SettingsHandler.SettingsObserver() {
            @Override
            public void valueChanged(SettingsHandler handler) {
                _tvValue.setText(getValue(_handler.getIntValue()).text);
            }
        });
    }

    private void showDropDown() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        String items[] = new String[_values.size()];
        for (int i=0; i<_values.size(); i++) {
            items[i] = _values.get(i).text;
        }
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                _handler.setValue(_values.get(i).value);
            }
        });
        builder.show();
    }

    public void addEnumValue(int value, String title) {
        EnumValue val = getValue(value);
        val.text = title;
        _tvValue.setText(getValue(_handler.getIntValue()).text);
    }

    private EnumValue getValue(int key) {
        for (EnumValue value: _values) {
            if (value.value == key) {
                return value;
            }
        }
        EnumValue value = new EnumValue();
        value.value = key;
        value.text = Integer.toString(key);
        _values.add(value);
        return value;
    }
}
