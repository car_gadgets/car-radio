package greenmesh.settings;

import com.bulleratz.bradio.InfoBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class InfoBarSettingHandler extends GenericListSettingsHandler<Integer> {
    public InfoBarSettingHandler(String name) {
        super(name);
    }

    @Override
    protected JSONObject toJsonObject(int index) throws JSONException {
        if (index < 0 || index >= getCount())
            return null;
        JSONObject value = new JSONObject();
        value.put("value", getValue(index));
        return value;
    }

    @Override
    public void onLoaded() {
        List<Integer> allValues = InfoBar.getItemList();
        for (int i=0; i<allValues.size(); i++) {
            if (!contains(allValues.get(i) | 0x80000000) && !contains(allValues.get(i))) {
                add(allValues.get(i));
            }
        }
    }

    @Override
    protected void addJsonObject(JSONObject object, int index) throws JSONException{
        int temp = object.getInt("value");
        for (int i=0; i<getCount(); i++) {
            if ((getValue(i) & 0x7fffffff) == (temp &0x7fffffff))
                return;
        }
        add(temp);
    }

    public void moveItemUp(int id) {
       for (int i=0; i<getCount(); i++) {
           int value = getValue(i).intValue() & 0x7fffffff;
           if ((id & 0x7fffffff) == value) {
               moveUp(i);
               return;
           }
       }
    }

    public void moveItemDown(int id) {
        for (int i=0; i<getCount(); i++) {
            int value = getValue(i).intValue() & 0x7fffffff;
            if ((id & 0x7fffffff) == value) {
                moveDown(i);
                return;
            }
        }
    }
}
