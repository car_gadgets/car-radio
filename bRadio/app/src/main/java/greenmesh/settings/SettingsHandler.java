package greenmesh.settings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public abstract class SettingsHandler {
    public static final String STR_VALUE = "Value";

    private String _name = "";
    private String _displayName = "";
    private String _group = "";
    private Object _value = null;


    public interface SettingsObserver {
        public void valueChanged(SettingsHandler handler);
    }

    private ArrayList<SettingsObserver> _observers = new ArrayList<>();

    public void addObserver(SettingsObserver observer) {
        if (observer != null && !_observers.contains(observer)) {
            _observers.add(observer);
        }
    }

    public void removeObserver(SettingsObserver observer) {
        while (_observers.contains(observer))
            _observers.remove(_observers);
    }

    protected final void notifyValueChanged() {
        for (SettingsObserver observer : _observers) {
            observer.valueChanged(this);
        }
    }

    public void formJsonObject(JSONObject object) throws JSONException {
        setValue(loadValue(object));
    }

    public abstract void toJsonObject(JSONObject object) throws JSONException;

    public boolean setValue(Object value) {
        if (acceptValue(value)) {
            if ((_value == null && value != null) || (_value != null && !_value.equals(value))) {
                _value = value;
                notifyValueChanged();
            }
            return true;
        }
        return false;
    }

    public SettingsHandler(String name) {
        _name = name;
    }

    protected abstract boolean acceptValue(Object value);

    public Object getValue() {
        return _value;
    }

    public String getName() {
        return _name;
    }

    public String getGroup() {
        return _group;
    }

    public void setGroup(String group) {
        if (group == null)
            _group = "";
        else
            _group = group;
    }

    public String getDisplayName() {
        return _displayName;
    }

    public void setDisplayName(String displayName) {
        if (displayName == null)
            _displayName = "";
        else
            _displayName = displayName;
    }

    protected abstract Object loadValue(JSONObject source) throws JSONException;
}
