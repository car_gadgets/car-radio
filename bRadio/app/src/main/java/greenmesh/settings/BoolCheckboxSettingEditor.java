package greenmesh.settings;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.bulleratz.bradio.R;

public class BoolCheckboxSettingEditor extends SettingsEditor {
    private BoolSettingsHandler _handler = null;

    public BoolCheckboxSettingEditor(Context context, BoolSettingsHandler handler) {
        super(context, handler);
        _handler = handler;
        inflate(context, R.layout.setting_panel_bool, this);
        onInflated();
        final Switch cbxValue = (Switch) findViewById(R.id.cbxValue);

        this.setClickable(true);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                cbxValue.toggle();
            }
        });

        if (_handler != null)
            cbxValue.setChecked(_handler.getBoolValue());

        cbxValue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (_handler != null && _handler.getBoolValue() != b) {
                    _handler.setValue(b);
                }
            }
        });
    }
}
