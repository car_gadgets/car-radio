package greenmesh.settings;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.bulleratz.bradio.InfoBar;
import com.bulleratz.bradio.R;

import java.util.ArrayList;

public class InfoBarSettingAdapter extends ArrayAdapter<Integer> {
    private final int allItems[] = new int[] {InfoBar.PI_VISIBLE, InfoBar.FREQ_VISIBLE, InfoBar.CLOCK_VISIABLE, InfoBar.ST_VISIBLE, InfoBar.TP_VISIBLE, InfoBar.TA_VISIBLE};
    InfoBarSettingHandler _settingsHandler = null;
    private Context _context = null;
    private ViewHolder _selectedItem = null;
    private int _selectedIndex = -1;
    private ArrayList<SelectedItemChangedObserver> _itemObservers = new ArrayList<>();

    public interface SelectedItemChangedObserver {
        void selectedItemChanged(int value);
    }

    private void setSelectedItem(ViewHolder holder) {
        if (holder != _selectedItem) {
            if (_selectedItem != null)
                _selectedItem.setSelected(false);
            _selectedItem = holder;
            if (holder != null) {
                _selectedIndex = holder.value & 0x7fffffff;
                holder.setSelected(true);
            }
            for (SelectedItemChangedObserver obs : _itemObservers) {
                obs.selectedItemChanged(holder.value);
            }
        }


    }


    public void addSelectedItemChangedObserver(SelectedItemChangedObserver observer) {
        if (!_itemObservers.contains(observer))
            _itemObservers.add(observer);
    }

    public InfoBarSettingAdapter(@NonNull Context context, InfoBarSettingHandler handler) {
        super(context, R.layout.staion_image_item_layout);
        _context = context;
        _settingsHandler = handler;
        _settingsHandler.addObserver(new SettingsHandler.SettingsObserver() {
            @Override
            public void valueChanged(SettingsHandler handler) {
                notifyDataSetChanged();
            }
        });
    }

    public int getIndex(int value) {
        for (int i=0; i < _settingsHandler.getCount(); i++) {
            int handlerValue = -1;
            if (_settingsHandler.getValue(i) != null) {
                handlerValue = _settingsHandler.getValue(i).intValue() & 0x7fffffff;
            }
            if (handlerValue == (value & 0x7fffffff)) {
                return i;
            }
        }
        return -1;
    }

    private class ViewHolder implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {
        public ImageView imageView = null;
        public TextView textView = null;
        public Switch cbx = null;
        public RelativeLayout layout = null;
        public int value = 0;
        public boolean selected = false;


        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            for (int i=0; i<_settingsHandler.getCount(); i++) {
                if ((_settingsHandler.getValue(i) & 0x7fffffff) == (ViewHolder.this.value & 0x7fffffff)) {
                    int newValue = this.value & 0x7fffffff;
                    if (b)
                        newValue |= 0x80000000;
                    _settingsHandler.setValue(i, newValue);
                    break;
                }
            }
        }

        @Override
        public void onClick(View view) {
            setSelectedItem(this);
        }

        public void setSelected(boolean selected) {
            if (layout != null) {
                if (selected) {
                    layout.setBackgroundColor(getContext().getResources().getColor(R.color.background_light_light));
                } else {
                    layout.setBackgroundColor(getContext().getResources().getColor(R.color.background_light));
                }
            }
        }
    }

    ViewHolder _selectedView = null;

    @Override
    public int getCount() {
        return _settingsHandler.getCount();
    }

    @Nullable
    @Override
    public Integer getItem(int position) {
        if (position < 0 || position > getCount())
            return 0;
        return _settingsHandler.getValue(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (view == null) {
            view = LayoutInflater.from(_context).inflate(R.layout.setting_panel_bool, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView)view.findViewById(R.id.tvDisplayName);
            viewHolder.imageView = (ImageView)view.findViewById(R.id.ivIcon);
            viewHolder.cbx = (Switch)view.findViewById(R.id.cbxValue);
            viewHolder.layout = (RelativeLayout) view.findViewById(R.id.settingBoolLayout);
            viewHolder.layout.setOnClickListener(viewHolder);
            view.setTag(viewHolder);
            viewHolder.cbx.setOnCheckedChangeListener(viewHolder);
        }
        viewHolder = (ViewHolder)view.getTag();
        viewHolder.value = getItem(position);
        viewHolder.textView.setText(getItemText(getItem(position) & 0x7fffffff));
        viewHolder.imageView.setImageDrawable(getItemDrawable(getItem(position) & 0x7fffffff));
        viewHolder.cbx.setChecked((getItem(position) & 0x80000000) != 0);
        if ((viewHolder.value & 0x7fffffff) == _selectedIndex) {
            setSelectedItem(viewHolder);
        } else {
            viewHolder.setSelected(false);
        }
        return view;
    }


    public String getItemText(int itenId) {
        switch (itenId) {
            case InfoBar.TA_VISIBLE:
                return "Traffic Announcemnt";
            case InfoBar.TP_VISIBLE:
                return "Traffic Programme";
            case InfoBar.ST_VISIBLE:
                return "Stereo Indicator";
            case InfoBar.CLOCK_VISIABLE:
                return "Clock";
            case InfoBar.PI_VISIBLE:
                return "Program Info";
            case InfoBar.FREQ_VISIBLE:
                return "Frequency";
            case InfoBar.PTY_VISIBLE:
                return "Program Type";
            case InfoBar.BOUQUET_SELECTION_VISIBLE:
                return "Bouquet Editor";
            default:
                return "";
        }
    }
    public Drawable getItemDrawable(int itemId) {
        switch (itemId) {
            case InfoBar.TA_VISIBLE:
                return _context.getResources().getDrawable(R.drawable.baseline_info_white_48);
            case InfoBar.ST_VISIBLE:
                return _context.getResources().getDrawable(R.drawable.stereo);
            case InfoBar.TP_VISIBLE:
                return _context.getResources().getDrawable(R.drawable.baseline_drive_eta_white_48);
            default:
                return null;
        }
    }

    public void moveSelectedItemUp() {
        _settingsHandler.moveItemDown(_selectedItem.value);
    }

    public void moveSelectedItemDown() {
        _settingsHandler.moveItemUp(_selectedItem.value);
    }
}
